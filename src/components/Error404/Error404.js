import React from "react";
import { Link } from "react-router-dom";

export default function Error404() {
	return (
		<div>
			<h1
				style={{
					marginTop: "0",
					padding: "25vh 7vw 0",
					textAlign: "center"
				}}>
				Doesn't look like anything to me.
			</h1>
			<Link
				to="/"
				class="button wood"
				style={{
					display: "block",
					margin: "auto",
					padding: "11px 9px 14px",
					textDecoration: "none",
					fontSize: "1.2em",
					width: "fit-content"
				}}>
				<span>Back to Homepage</span>
			</Link>
		</div>
	);
}
