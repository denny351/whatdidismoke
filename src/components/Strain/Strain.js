import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./strain.css";
import axios from "axios";

import Wrapper from '../Hoc/Wrapper';

const API_KEY = process.env.REACT_APP_STRAIN_API_KEY;

class Strain extends Component {
	state = {
		name: null,
		race: null,
		description: null,
		positives: [],
		negatives: [],
		medicals: [],
		flavors: []
	};

	componentDidMount = () => {
		let { id, name } = this.props.match.params;

		axios
			.all([
				axios.get(
                    `https://strainapi.evanbusse.com/${API_KEY}/strains/search/name/${name}`
				),
				axios.get(
                    `https://strainapi.evanbusse.com/${API_KEY}/strains/data/effects/${id}`
				),
				axios.get(
                    `https://strainapi.evanbusse.com/${API_KEY}/strains/data/flavors/${id}`
				)
			])
			.then(
				axios.spread((strain, effects, flavors) => {
					this.setState({
						name: strain.data[0].name,
						race: strain.data[0].race,
						description: strain.data[0].desc,
						positives: effects.data.positive,
						negatives: effects.data.negative,
						medicals: effects.data.medical,
						flavors: flavors.data
					});
				})
			);
	};

	render() {
		var {
			name,
			race,
			description,
			positives,
			negatives,
			medicals,
			flavors
		} = this.state;

		const listThings = list => {
			if (list.length <= 0) {
				return <li>No information</li>;
			} else {
				return list.map((item, i) => {
					return <li key={i}>{item}</li>;
				});
			}
        };
		return (
            <Wrapper>
			<div className="container">
                <Link id="back" to={{
                        pathname: '/',
                        state: { prev: false },
                    }} >&lt; Back to search</Link>
				<h1>{name}</h1>
				<h2>{race}</h2>
				{description != null && <p>{description}</p>}
				<div className="listsContainer">
					<div className="listBox wood-back">
						<h4>Associated Flavors:</h4>
						<ul>{listThings(flavors)}</ul>
					</div>
                        <div className="listBox wood-back">
						<h4>Positives effects: </h4>
						<ul>{listThings(positives)}</ul>
					</div>
                        <div className="listBox wood-back">
						<h4>Negative effects:</h4>
						<ul>{listThings(negatives)}</ul>
					</div>
                        <div className="listBox wood-back">
						<h4>Medical uses:</h4>
						<ul>{listThings(medicals)}</ul>
					</div>
				</div>
				<br />
			</div>
            </Wrapper>
		);
	}
}

export default Strain;
