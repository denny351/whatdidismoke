import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from "react-transition-group";
import ReactGA from "react-ga";

import Wrapper from '../Hoc/Wrapper';

import { searchStrains } from "../../js/search.js";
import "./homepage.css";

ReactGA.initialize("UA-128167939-1");

class HomePage extends Component {
	state = {
		strains: []
	};

	searchHandler = e => {
		const searchInput = e.target.value;
		if (searchInput === "") {
			this.setState({ strains: [] });
		} else {
			var results = searchStrains(searchInput);
			this.setState({ strains: results });
		}
	};

	enterHandler = e => {
        if (e.charCode == 13) {
            e.preventDefault();
            e.stopPropagation();
            e.target.blur();
        }
    }

    blurHandler = e => {
        ReactGA.event({
            category: 'Search',
            action: 'Searched',
            label: e.target.value
        });
    }

	render() {
		const listStrains = this.state.strains.map(item => {
			let { name, id } = item;
			return (
				<CSSTransition timeout={400} classNames="slide" key={id}>
					<Link
						to={{
							pathname: `/strain/${id}/${name}`,
							state: { prev: true }
						}}>
						{name}
					</Link>
				</CSSTransition>
			);
		});

		return (
			<Wrapper>
				<div id="index">
					<h1>What did I smoke.</h1>
					<h3>Type in your strain below</h3>
					<input
						type="text"
						onChange={this.searchHandler}
						onKeyPress={this.enterHandler}
						onBlur={this.blurHandler}
						placeholder="ex. granddaddy purple"
					/>
					<TransitionGroup>{listStrains}</TransitionGroup>
					<br />
					<br />
					<br />
					<br />
					<br />
					<br />
					<br />
                    <p id="mobile-help">Tap screen to puff smoke</p>
				</div>
			</Wrapper>
		);
	}
}

export default HomePage;