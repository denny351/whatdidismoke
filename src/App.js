import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import ReactGA from "react-ga";

import "./App.css";
import { makesmoke } from "./js/smoke";

import HomePage from "./components/HomePage/HomePage";
import Strain from "./components/Strain/Strain";
import Error404 from "./components/Error404/Error404";

function initializeReactGA() {
    ReactGA.initialize('UA-128167939-1');
    ReactGA.pageview('/');
}

initializeReactGA();

class App extends Component {
    state = {
        smoke: true,
        smokeMsg: "Turn off smoke"
    }

	componentDidMount() {
		const canvas = this.refs.canvas;
		const ctx = canvas.getContext("2d");

		makesmoke(canvas, ctx);
    }

    smokeToggle = () => {
        if(this.state.smoke == true){
            this.setState({smoke: false, smokeMsg: 'Smoke up again'});
        } else {
            this.setState({ smoke: true, smokeMsg: 'Turn off smoke' });
        }
    }

	render() {

		const supportsHistory = "pushState" in window.history;

		return (
			<React.Fragment>
                <div id="smokeToggle" className="button wood" onClick={this.smokeToggle}>
                    <div>{this.state.smokeMsg}</div>
                </div>
				<canvas ref="canvas" id="canvas" style={{display: this.state.smoke ? 'block' : 'none' }} />
				<BrowserRouter forceRefresh={!supportsHistory}>
					<Route
						render={({ location }) => {
							const { pathname } = location;
							return (
								<TransitionGroup>
									<CSSTransition
										key={pathname}
										classNames="page"
										timeout={{ enter: 800, exit: 800 }}>
										<Route
											location={location}
											render={() => (
												<Switch>
													<Route exact path="/" component={HomePage} />
													<Route
														exact
														path="/strain/:id/:name"
														component={Strain}
													/>
                                                    <Route component={Error404}/>
												</Switch>
											)}
										/>
									</CSSTransition>
								</TransitionGroup>
							);
						}}
					/>
				</BrowserRouter>
			</React.Fragment>
		);
	}
}

export default App;
