const strains = [
    {
        "name": "Afpak",
        "id": 1
    },
    {
        "name": "African",
        "id": 2
    },
    {
        "name": "Afternoon Delight",
        "id": 3
    },
    {
        "name": "Afwreck",
        "id": 4
    },
    {
        "name": "Agent Orange",
        "id": 5
    },
    {
        "name": "Agent Tangie",
        "id": 6
    },
    {
        "name": "Alaska",
        "id": 8
    },
    {
        "name": "Alaska Thunder Grape",
        "id": 9
    },
    {
        "name": "Alaskan Ice",
        "id": 10
    },
    {
        "name": "Alaskan Thunder Fuck",
        "id": 11
    },
    {
        "name": "Albert Walker",
        "id": 12
    },
    {
        "name": "Alchemy",
        "id": 13
    },
    {
        "name": "Alf",
        "id": 14
    },
    {
        "name": "Alice in Wonderland",
        "id": 15
    },
    {
        "name": "Alien Abduction",
        "id": 16
    },
    {
        "name": "Alien Apparition",
        "id": 17
    },
    {
        "name": "Alien Asshat",
        "id": 18
    },
    {
        "name": "Alien Bubba",
        "id": 19
    },
    {
        "name": "Alien Dawg",
        "id": 20
    },
    {
        "name": "Alien Dutchess",
        "id": 21
    },
    {
        "name": "Alien Hallucination",
        "id": 22
    },
    {
        "name": "Alien Inferno",
        "id": 23
    },
    {
        "name": "Alien Kush",
        "id": 24
    },
    {
        "name": "Alien OG",
        "id": 25
    },
    {
        "name": "Alien Reunion",
        "id": 26
    },
    {
        "name": "Alien Rift",
        "id": 27
    },
    {
        "name": "Alien Rock Candy",
        "id": 28
    },
    {
        "name": "Alien Sour Apple",
        "id": 29
    },
    {
        "name": "Alien Stardawg",
        "id": 30
    },
    {
        "name": "Alien Technology",
        "id": 31
    },
    {
        "name": "Aliens On Moonshine",
        "id": 32
    },
    {
        "name": "Allen Wrench",
        "id": 33
    },
    {
        "name": "Allkush",
        "id": 34
    },
    {
        "name": "Aloha",
        "id": 35
    },
    {
        "name": "Aloha Limone",
        "id": 36
    },
    {
        "name": "Alohaberry",
        "id": 37
    },
    {
        "name": "Alpha Blue",
        "id": 38
    },
    {
        "name": "Alpha Express",
        "id": 40
    },
    {
        "name": "Alpha OG",
        "id": 41
    },
    {
        "name": "Alpine Blue",
        "id": 42
    },
    {
        "name": "Alpine Star",
        "id": 43
    },
    {
        "name": "Ambrosia",
        "id": 44
    },
    {
        "name": "American Dream",
        "id": 45
    },
    {
        "name": "American Kush",
        "id": 46
    },
    {
        "name": "Americano",
        "id": 47
    },
    {
        "name": "Amnesia",
        "id": 50
    },
    {
        "name": "Amnesia Haze",
        "id": 52
    },
    {
        "name": "Ancient Kush",
        "id": 53
    },
    {
        "name": "Ancient OG",
        "id": 54
    },
    {
        "name": "Anesthesia",
        "id": 55
    },
    {
        "name": "Angel OG",
        "id": 56
    },
    {
        "name": "Animal Cookies",
        "id": 57
    },
    {
        "name": "Anonymous OG",
        "id": 58
    },
    {
        "name": "Ape Shit",
        "id": 59
    },
    {
        "name": "Apollo 11",
        "id": 60
    },
    {
        "name": "Apollo 13",
        "id": 61
    },
    {
        "name": "Appalachia",
        "id": 62
    },
    {
        "name": "Appalachian Power",
        "id": 63
    },
    {
        "name": "Apple Jack",
        "id": 64
    },
    {
        "name": "Apple Kush",
        "id": 65
    },
    {
        "name": "Appleberry",
        "id": 66
    },
    {
        "name": "Arctic Sun",
        "id": 68
    },
    {
        "name": "ArcticBlue",
        "id": 69
    },
    {
        "name": "Area 51",
        "id": 70
    },
    {
        "name": "Argyle",
        "id": 71
    },
    {
        "name": "Arjan's Strawberry Haze",
        "id": 72
    },
    {
        "name": "Arjan's Ultra Haze #1",
        "id": 73
    },
    {
        "name": "Armageddon",
        "id": 74
    },
    {
        "name": "Armagnac",
        "id": 75
    },
    {
        "name": "Ash",
        "id": 76
    },
    {
        "name": "Asian Fantasy",
        "id": 77
    },
    {
        "name": "Aspen OG",
        "id": 78
    },
    {
        "name": "Astroboy",
        "id": 79
    },
    {
        "name": "Athabasca",
        "id": 80
    },
    {
        "name": "Atmosphere",
        "id": 81
    },
    {
        "name": "Atomic Goat",
        "id": 82
    },
    {
        "name": "Atomic Northern Lights",
        "id": 83
    },
    {
        "name": "Atomical Haze",
        "id": 84
    },
    {
        "name": "Aurora Borealis",
        "id": 85
    },
    {
        "name": "Aurora Indica",
        "id": 86
    },
    {
        "name": "Avalon",
        "id": 87
    },
    {
        "name": "Avi",
        "id": 88
    },
    {
        "name": "Avi-Dekel",
        "id": 89
    },
    {
        "name": "Azure Haze",
        "id": 90
    },
    {
        "name": "B-52",
        "id": 91
    },
    {
        "name": "B-Witched",
        "id": 92
    },
    {
        "name": "B.B. King",
        "id": 93
    },
    {
        "name": "B4",
        "id": 94
    },
    {
        "name": "BC Big Bud",
        "id": 95
    },
    {
        "name": "BC Roadkill",
        "id": 96
    },
    {
        "name": "BC Sweet Tooth",
        "id": 97
    },
    {
        "name": "BOG Bubble",
        "id": 98
    },
    {
        "name": "BSC",
        "id": 99
    },
    {
        "name": "Bakerstreet",
        "id": 100
    },
    {
        "name": "Balmoral",
        "id": 101
    },
    {
        "name": "Banana Candy",
        "id": 102
    },
    {
        "name": "Banana Diesel",
        "id": 103
    },
    {
        "name": "Banana Kush",
        "id": 104
    },
    {
        "name": "Banana OG",
        "id": 105
    },
    {
        "name": "Banana Peel",
        "id": 106
    },
    {
        "name": "Bananas",
        "id": 107
    },
    {
        "name": "Bandana",
        "id": 108
    },
    {
        "name": "Bango",
        "id": 109
    },
    {
        "name": "Barbara Bud",
        "id": 110
    },
    {
        "name": "Batman OG",
        "id": 112
    },
    {
        "name": "Bay 11",
        "id": 113
    },
    {
        "name": "Bay Dream",
        "id": 114
    },
    {
        "name": "Beastmode 2.0",
        "id": 115
    },
    {
        "name": "Beastmode OG",
        "id": 116
    },
    {
        "name": "Bedford Glue",
        "id": 117
    },
    {
        "name": "Bedica",
        "id": 118
    },
    {
        "name": "Bediol",
        "id": 119
    },
    {
        "name": "Bell Ringer",
        "id": 120
    },
    {
        "name": "Belladonna",
        "id": 121
    },
    {
        "name": "Berkeley",
        "id": 122
    },
    {
        "name": "Berry Bomb",
        "id": 123
    },
    {
        "name": "Berry Noir",
        "id": 124
    },
    {
        "name": "Berry OG",
        "id": 125
    },
    {
        "name": "Berry White",
        "id": 126
    },
    {
        "name": "Bertberry Cheesecake",
        "id": 128
    },
    {
        "name": "Bettie Page",
        "id": 129
    },
    {
        "name": "Bhang 007 OG",
        "id": 130
    },
    {
        "name": "Bhang Afgoo",
        "id": 135
    },
    {
        "name": "Bhang Arjan's Strawberry Haze",
        "id": 138
    },
    {
        "name": "Bhang Blackberry Kush",
        "id": 143
    },
    {
        "name": "Bhang Bubba Kush",
        "id": 145
    },
    {
        "name": "Bhang Cali Orange",
        "id": 147
    },
    {
        "name": "Bhang Casey Jones",
        "id": 148
    },
    {
        "name": "Bhang Cheese",
        "id": 150
    },
    {
        "name": "Bhang ChemDawg",
        "id": 151
    },
    {
        "name": "Bhang Cherry Kola",
        "id": 152
    },
    {
        "name": "Bhang Chocolope",
        "id": 156
    },
    {
        "name": "Bhang Diablo OG",
        "id": 159
    },
    {
        "name": "Bhang Durban Poison",
        "id": 161
    },
    {
        "name": "Bhang Dutch Treat",
        "id": 162
    },
    {
        "name": "Bhang Girl Scout Cookies",
        "id": 166
    },
    {
        "name": "Bhang Granddaddy Purple",
        "id": 167
    },
    {
        "name": "Bhang Grape Ape",
        "id": 168
    },
    {
        "name": "Bhang Harlequin",
        "id": 170
    },
    {
        "name": "Bhang Hawaiian Haze",
        "id": 171
    },
    {
        "name": "Bhang Holy Grail",
        "id": 175
    },
    {
        "name": "Bhang Island Sweet Skunk",
        "id": 177
    },
    {
        "name": "Bhang J-1",
        "id": 178
    },
    {
        "name": "Bhang Jack Herer",
        "id": 180
    },
    {
        "name": "Bhang Juicy Fruit",
        "id": 181
    },
    {
        "name": "Bhang Kali Mist",
        "id": 183
    },
    {
        "name": "Bhang King's Kush",
        "id": 184
    },
    {
        "name": "Bhang Lavender Kush",
        "id": 187
    },
    {
        "name": "Bhang Lemon Haze",
        "id": 189
    },
    {
        "name": "Bhang Lemon Skunk",
        "id": 190
    },
    {
        "name": "Bhang Mango OG",
        "id": 192
    },
    {
        "name": "Bhang Master Kush",
        "id": 193
    },
    {
        "name": "Bhang Maui Waui",
        "id": 194
    },
    {
        "name": "Bhang Mr. Nice",
        "id": 196
    },
    {
        "name": "Bhang Northern Lights",
        "id": 198
    },
    {
        "name": "Bhang Ogre",
        "id": 200
    },
    {
        "name": "Bhang Perfecto Hybrid",
        "id": 202
    },
    {
        "name": "Bhang Perfecto Indica",
        "id": 203
    },
    {
        "name": "Bhang Pineapple Express",
        "id": 205
    },
    {
        "name": "Bhang Pineapple Kush",
        "id": 206
    },
    {
        "name": "Bhang Purple Diesel",
        "id": 211
    },
    {
        "name": "Bhang Purple Erkle",
        "id": 212
    },
    {
        "name": "Bhang Skunk #1",
        "id": 221
    },
    {
        "name": "Bhang Skywalker OG",
        "id": 222
    },
    {
        "name": "Bhang Sour Diesel",
        "id": 223
    },
    {
        "name": "Bhang Sour OG",
        "id": 224
    },
    {
        "name": "Bhang Strawberry Cough",
        "id": 225
    },
    {
        "name": "Bhang Strawberry Diesel",
        "id": 226
    },
    {
        "name": "Bhang Super Lemon Haze",
        "id": 227
    },
    {
        "name": "Bhang Tangie",
        "id": 232
    },
    {
        "name": "Bhang Triple Berry Goo",
        "id": 234
    },
    {
        "name": "Bhang White Diesel",
        "id": 236
    },
    {
        "name": "Bhang White Widow",
        "id": 237
    },
    {
        "name": "Bhang XJ-13",
        "id": 239
    },
    {
        "name": "Bhang Yoda OG",
        "id": 240
    },
    {
        "name": "Bianca",
        "id": 241
    },
    {
        "name": "Biddy Early",
        "id": 242
    },
    {
        "name": "Big Band",
        "id": 243
    },
    {
        "name": "Big Bang",
        "id": 244
    },
    {
        "name": "Big Bud",
        "id": 245
    },
    {
        "name": "Big Buddha Cheese",
        "id": 246
    },
    {
        "name": "Big Holy Nina",
        "id": 247
    },
    {
        "name": "Big Kush",
        "id": 248
    },
    {
        "name": "Big Mac",
        "id": 249
    },
    {
        "name": "Big Skunk Korean",
        "id": 250
    },
    {
        "name": "Big Sky OG",
        "id": 251
    },
    {
        "name": "Big Smooth",
        "id": 252
    },
    {
        "name": "Big Sur Holy Bud",
        "id": 253
    },
    {
        "name": "Big White",
        "id": 254
    },
    {
        "name": "Big Wreck",
        "id": 255
    },
    {
        "name": "Biker Le’Blanc",
        "id": 256
    },
    {
        "name": "Bio-Diesel",
        "id": 257
    },
    {
        "name": "Bio-Jesus",
        "id": 258
    },
    {
        "name": "Biochem",
        "id": 259
    },
    {
        "name": "Birthday Cake Kush",
        "id": 261
    },
    {
        "name": "Black '84",
        "id": 262
    },
    {
        "name": "Black Afghan",
        "id": 263
    },
    {
        "name": "Black Betty",
        "id": 264
    },
    {
        "name": "Black Bubba",
        "id": 265
    },
    {
        "name": "Black Cherry Cheesecake",
        "id": 266
    },
    {
        "name": "Black Cherry OG",
        "id": 267
    },
    {
        "name": "Black Cherry Pie",
        "id": 268
    },
    {
        "name": "Black Cherry Soda",
        "id": 269
    },
    {
        "name": "Black Dahlia",
        "id": 270
    },
    {
        "name": "Black Diamond",
        "id": 271
    },
    {
        "name": "Black Diesel",
        "id": 272
    },
    {
        "name": "Black Domina",
        "id": 273
    },
    {
        "name": "Black Haze",
        "id": 274
    },
    {
        "name": "Black Ice",
        "id": 275
    },
    {
        "name": "Black Jack",
        "id": 276
    },
    {
        "name": "Black Label Kush",
        "id": 277
    },
    {
        "name": "Black Lime Special Reserve",
        "id": 278
    },
    {
        "name": "Black Magic",
        "id": 279
    },
    {
        "name": "Black Magic Kush",
        "id": 280
    },
    {
        "name": "Black Mamba",
        "id": 281
    },
    {
        "name": "Black Russian",
        "id": 282
    },
    {
        "name": "Black Tuna",
        "id": 283
    },
    {
        "name": "Black Velvet",
        "id": 284
    },
    {
        "name": "Black Widow",
        "id": 285
    },
    {
        "name": "Blackberry",
        "id": 286
    },
    {
        "name": "Blackberry Bubble",
        "id": 287
    },
    {
        "name": "Blackberry Chem OG",
        "id": 288
    },
    {
        "name": "Blackberry Cream",
        "id": 289
    },
    {
        "name": "Blackberry Diesel",
        "id": 290
    },
    {
        "name": "Blackberry Dream",
        "id": 291
    },
    {
        "name": "Blackberry Hashplant",
        "id": 293
    },
    {
        "name": "Blackberry Haze",
        "id": 294
    },
    {
        "name": "Blackberry Kush",
        "id": 295
    },
    {
        "name": "Blackberry Lime Haze",
        "id": 296
    },
    {
        "name": "Blackberry Pie",
        "id": 297
    },
    {
        "name": "Blackberry Rhino",
        "id": 298
    },
    {
        "name": "Blackberry Trainwreck",
        "id": 299
    },
    {
        "name": "Blackberry x Blueberry",
        "id": 300
    },
    {
        "name": "Blackwater",
        "id": 301
    },
    {
        "name": "Blaze",
        "id": 302
    },
    {
        "name": "Blissful Wizard",
        "id": 303
    },
    {
        "name": "Blob OG",
        "id": 304
    },
    {
        "name": "Blockhead",
        "id": 305
    },
    {
        "name": "Bloo's Kloos",
        "id": 306
    },
    {
        "name": "Bloodhound",
        "id": 307
    },
    {
        "name": "Blowfish",
        "id": 308
    },
    {
        "name": "Blucifer",
        "id": 309
    },
    {
        "name": "Blue Afghani",
        "id": 310
    },
    {
        "name": "Blue Alien",
        "id": 311
    },
    {
        "name": "Blue Bastard",
        "id": 312
    },
    {
        "name": "Blue Bayou",
        "id": 313
    },
    {
        "name": "Blue Blood",
        "id": 314
    },
    {
        "name": "Blue Boy",
        "id": 315
    },
    {
        "name": "Blue Buddha",
        "id": 316
    },
    {
        "name": "Blue Champagne",
        "id": 317
    },
    {
        "name": "Blue Cheese",
        "id": 318
    },
    {
        "name": "Blue Cinex",
        "id": 319
    },
    {
        "name": "Blue Cookies",
        "id": 320
    },
    {
        "name": "Blue Crack",
        "id": 321
    },
    {
        "name": "Blue Diamond",
        "id": 322
    },
    {
        "name": "Blue Diesel",
        "id": 323
    },
    {
        "name": "Blue Dot",
        "id": 324
    },
    {
        "name": "Blue Dragon",
        "id": 325
    },
    {
        "name": "Blue Dream",
        "id": 326
    },
    {
        "name": "Blue Dynamite",
        "id": 327
    },
    {
        "name": "Blue Frost",
        "id": 328
    },
    {
        "name": "Blue Galaxy",
        "id": 329
    },
    {
        "name": "Blue God",
        "id": 330
    },
    {
        "name": "Blue Goo",
        "id": 331
    },
    {
        "name": "Blue Hash",
        "id": 332
    },
    {
        "name": "Blue Hawaiian",
        "id": 333
    },
    {
        "name": "Blue Haze",
        "id": 334
    },
    {
        "name": "Blue Heron",
        "id": 335
    },
    {
        "name": "Blue Ivy",
        "id": 336
    },
    {
        "name": "Blue Jay Way",
        "id": 337
    },
    {
        "name": "Blue Kiss",
        "id": 338
    },
    {
        "name": "Blue Knight",
        "id": 339
    },
    {
        "name": "Blue Kripple",
        "id": 340
    },
    {
        "name": "Blue Kush",
        "id": 341
    },
    {
        "name": "Blue Lights",
        "id": 342
    },
    {
        "name": "Blue Magic",
        "id": 343
    },
    {
        "name": "Blue Magoo",
        "id": 344
    },
    {
        "name": "Blue Monster",
        "id": 345
    },
    {
        "name": "Blue Moon Rocks",
        "id": 346
    },
    {
        "name": "Blue Moonshine",
        "id": 347
    },
    {
        "name": "Blue Mountain Fire",
        "id": 348
    },
    {
        "name": "Blue Mystic",
        "id": 349
    },
    {
        "name": "Blue Nightmare",
        "id": 350
    },
    {
        "name": "Blue OG",
        "id": 352
    },
    {
        "name": "Blue Ox",
        "id": 353
    },
    {
        "name": "Blue Persuasion",
        "id": 354
    },
    {
        "name": "Blue Power",
        "id": 355
    },
    {
        "name": "Blue Rhino",
        "id": 356
    },
    {
        "name": "Blue Satellite",
        "id": 357
    },
    {
        "name": "Blue Sky",
        "id": 358
    },
    {
        "name": "Blue Steel",
        "id": 359
    },
    {
        "name": "Blue Thai",
        "id": 360
    },
    {
        "name": "Blue Trainwreck",
        "id": 361
    },
    {
        "name": "Blue Treat",
        "id": 363
    },
    {
        "name": "Blue Velvet",
        "id": 364
    },
    {
        "name": "Blue Venom",
        "id": 365
    },
    {
        "name": "Blue Widow",
        "id": 366
    },
    {
        "name": "Blue Wonder",
        "id": 367
    },
    {
        "name": "Blue Wreck",
        "id": 368
    },
    {
        "name": "Blue Zombie",
        "id": 369
    },
    {
        "name": "Blueberry",
        "id": 370
    },
    {
        "name": "Blueberry AK",
        "id": 371
    },
    {
        "name": "Blueberry Blast",
        "id": 372
    },
    {
        "name": "Blueberry Cheesecake",
        "id": 373
    },
    {
        "name": "Blueberry Diesel",
        "id": 374
    },
    {
        "name": "Blueberry Dream",
        "id": 375
    },
    {
        "name": "Blueberry Essence",
        "id": 376
    },
    {
        "name": "Blueberry Haze",
        "id": 377
    },
    {
        "name": "Blueberry Headband",
        "id": 378
    },
    {
        "name": "Blueberry Jack",
        "id": 379
    },
    {
        "name": "Blueberry Kush",
        "id": 380
    },
    {
        "name": "Blueberry Lambsbread",
        "id": 381
    },
    {
        "name": "Blueberry Muffins",
        "id": 382
    },
    {
        "name": "Blueberry OG",
        "id": 383
    },
    {
        "name": "Blueberry Pancakes",
        "id": 384
    },
    {
        "name": "Blueberry Pie",
        "id": 385
    },
    {
        "name": "Blueberry Silvertip",
        "id": 386
    },
    {
        "name": "Blueberry Skunk",
        "id": 387
    },
    {
        "name": "Blueberry Waltz",
        "id": 388
    },
    {
        "name": "Blueberry Yum Yum",
        "id": 389
    },
    {
        "name": "Blues",
        "id": 390
    },
    {
        "name": "Bluniverse",
        "id": 392
    },
    {
        "name": "Blurple",
        "id": 393
    },
    {
        "name": "Blush",
        "id": 394
    },
    {
        "name": "Bob Saget",
        "id": 395
    },
    {
        "name": "Bogart",
        "id": 396
    },
    {
        "name": "Boggle Gum",
        "id": 397
    },
    {
        "name": "Booger",
        "id": 398
    },
    {
        "name": "Bootlegger",
        "id": 400
    },
    {
        "name": "Bordello",
        "id": 401
    },
    {
        "name": "Boss Hogg",
        "id": 402
    },
    {
        "name": "Boysenberry ",
        "id": 403
    },
    {
        "name": "Brains Damage",
        "id": 404
    },
    {
        "name": "Brainstorm Haze",
        "id": 405
    },
    {
        "name": "Brainwreck",
        "id": 406
    },
    {
        "name": "Brand X",
        "id": 407
    },
    {
        "name": "Brazil Amazonia",
        "id": 409
    },
    {
        "name": "Brian Berry Cough",
        "id": 410
    },
    {
        "name": "Brooklyn Mango",
        "id": 413
    },
    {
        "name": "Bruce Banner",
        "id": 414
    },
    {
        "name": "Bruce Banner #3",
        "id": 415
    },
    {
        "name": "Bubba Kush",
        "id": 418
    },
    {
        "name": "Bubba OG",
        "id": 419
    },
    {
        "name": "Bubba Tom Hayes",
        "id": 420
    },
    {
        "name": "Bubba x Skunk",
        "id": 421
    },
    {
        "name": "Bubba's Gift",
        "id": 423
    },
    {
        "name": "Bubble Cheese",
        "id": 424
    },
    {
        "name": "Bubble Gum",
        "id": 425
    },
    {
        "name": "Bubbleberry",
        "id": 426
    },
    {
        "name": "Bubblegum Kush",
        "id": 427
    },
    {
        "name": "Bubblegun",
        "id": 428
    },
    {
        "name": "Bubblicious",
        "id": 429
    },
    {
        "name": "Buckeye Purple",
        "id": 430
    },
    {
        "name": "Buddha Haze",
        "id": 431
    },
    {
        "name": "Buddha Tahoe",
        "id": 432
    },
    {
        "name": "Buddha's Hand",
        "id": 433
    },
    {
        "name": "Buddha's Sister",
        "id": 434
    },
    {
        "name": "Buffalo Bill",
        "id": 435
    },
    {
        "name": "Bugatti OG",
        "id": 436
    },
    {
        "name": "Burkle",
        "id": 437
    },
    {
        "name": "Burmese Kush",
        "id": 438
    },
    {
        "name": "Butter OG",
        "id": 439
    },
    {
        "name": "Butterscotch",
        "id": 440
    },
    {
        "name": "C13 Haze",
        "id": 441
    },
    {
        "name": "C3PO",
        "id": 443
    },
    {
        "name": "C4",
        "id": 444
    },
    {
        "name": "CBD Critical Cure",
        "id": 445
    },
    {
        "name": "CBD Kush",
        "id": 446
    },
    {
        "name": "CBD Mango Haze",
        "id": 448
    },
    {
        "name": "CBD OX",
        "id": 449
    },
    {
        "name": "CBD Shark",
        "id": 450
    },
    {
        "name": "CHR Super OG",
        "id": 451
    },
    {
        "name": "Cabbage Patch",
        "id": 452
    },
    {
        "name": "Cactus",
        "id": 453
    },
    {
        "name": "Cactus Cooler",
        "id": 454
    },
    {
        "name": "Cadillac Purple",
        "id": 455
    },
    {
        "name": "Cali Gold",
        "id": 456
    },
    {
        "name": "Cali Jack",
        "id": 457
    },
    {
        "name": "Cali Kush",
        "id": 458
    },
    {
        "name": "California Grapefruit",
        "id": 459
    },
    {
        "name": "California Orange",
        "id": 461
    },
    {
        "name": "California Sour",
        "id": 462
    },
    {
        "name": "Cambodian",
        "id": 463
    },
    {
        "name": "Cambodian Haze",
        "id": 464
    },
    {
        "name": "Candy Cane",
        "id": 465
    },
    {
        "name": "Candy Jack",
        "id": 466
    },
    {
        "name": "Candyland",
        "id": 467
    },
    {
        "name": "Canna-Tsu",
        "id": 468
    },
    {
        "name": "CannaSutra",
        "id": 469
    },
    {
        "name": "Cannadential",
        "id": 470
    },
    {
        "name": "Cannalope Haze",
        "id": 471
    },
    {
        "name": "Cannalope Kush",
        "id": 472
    },
    {
        "name": "Cannatonic",
        "id": 473
    },
    {
        "name": "Capers",
        "id": 474
    },
    {
        "name": "Captain America OG",
        "id": 476
    },
    {
        "name": "Captain's Cake",
        "id": 477
    },
    {
        "name": "Caramel Candy Kush",
        "id": 478
    },
    {
        "name": "Caramel Kona Coffee Kush",
        "id": 479
    },
    {
        "name": "Caramelicious",
        "id": 480
    },
    {
        "name": "Caramella",
        "id": 481
    },
    {
        "name": "Caramelo",
        "id": 482
    },
    {
        "name": "Carl Sagan",
        "id": 483
    },
    {
        "name": "Carnival",
        "id": 484
    },
    {
        "name": "Cascadia Kush",
        "id": 485
    },
    {
        "name": "Casey Jones",
        "id": 486
    },
    {
        "name": "Cashus Clay",
        "id": 487
    },
    {
        "name": "Casper OG",
        "id": 488
    },
    {
        "name": "Cat Piss",
        "id": 489
    },
    {
        "name": "Cataract Kush",
        "id": 490
    },
    {
        "name": "Catfish",
        "id": 491
    },
    {
        "name": "Cello Sweet OG",
        "id": 492
    },
    {
        "name": "Central American",
        "id": 493
    },
    {
        "name": "Cerebro Haze",
        "id": 494
    },
    {
        "name": "Chairman Chem Sou",
        "id": 495
    },
    {
        "name": "Champagne Kush",
        "id": 496
    },
    {
        "name": "Charlie Sheen",
        "id": 497
    },
    {
        "name": "Charlotte's Web",
        "id": 498
    },
    {
        "name": "Cheese",
        "id": 499
    },
    {
        "name": "Cheese Candy",
        "id": 500
    },
    {
        "name": "Cheese Quake",
        "id": 501
    },
    {
        "name": "Cheeseburger",
        "id": 502
    },
    {
        "name": "Cheesewreck",
        "id": 503
    },
    {
        "name": "Chem Crush",
        "id": 504
    },
    {
        "name": "Chem D.O.G.",
        "id": 505
    },
    {
        "name": "Chem Scout",
        "id": 507
    },
    {
        "name": "Chem Valley Kush",
        "id": 508
    },
    {
        "name": "Chem's Sister",
        "id": 509
    },
    {
        "name": "ChemWreck",
        "id": 510
    },
    {
        "name": "Chemdawg",
        "id": 511
    },
    {
        "name": "Chemdawg #4 x Alien OG Kush",
        "id": 512
    },
    {
        "name": "Chemdawg 4",
        "id": 513
    },
    {
        "name": "Chemdawg 91",
        "id": 514
    },
    {
        "name": "Chemdawg Sour Diesel",
        "id": 515
    },
    {
        "name": "Chemmy Jones",
        "id": 516
    },
    {
        "name": "Chemnesia",
        "id": 517
    },
    {
        "name": "Chemo",
        "id": 518
    },
    {
        "name": "Chernobyl",
        "id": 520
    },
    {
        "name": "Cherries Jubilee",
        "id": 521
    },
    {
        "name": "Cherry AK-47",
        "id": 522
    },
    {
        "name": "Cherry Bomb",
        "id": 523
    },
    {
        "name": "Cherry Cookies",
        "id": 524
    },
    {
        "name": "Cherry Cream Pie",
        "id": 525
    },
    {
        "name": "Cherry Diesel",
        "id": 526
    },
    {
        "name": "Cherry Durban Poison",
        "id": 527
    },
    {
        "name": "Cherry Grapefruit",
        "id": 528
    },
    {
        "name": "Cherry Kola",
        "id": 529
    },
    {
        "name": "Cherry Kush",
        "id": 530
    },
    {
        "name": "Cherry Limeade",
        "id": 531
    },
    {
        "name": "Cherry OG",
        "id": 532
    },
    {
        "name": "Cherry Pie",
        "id": 533
    },
    {
        "name": "Cherry Pie Kush",
        "id": 534
    },
    {
        "name": "Cherry Sauce",
        "id": 535
    },
    {
        "name": "Cherry Sherbet",
        "id": 536
    },
    {
        "name": "Cherry Skunk",
        "id": 537
    },
    {
        "name": "Cherry Thunder Fuck",
        "id": 538
    },
    {
        "name": "Chiesel",
        "id": 539
    },
    {
        "name": "China Yunnan",
        "id": 540
    },
    {
        "name": "Chiquita Banana",
        "id": 541
    },
    {
        "name": "Chloe",
        "id": 542
    },
    {
        "name": "Chocolate Chunk",
        "id": 544
    },
    {
        "name": "Chocolate Diesel",
        "id": 545
    },
    {
        "name": "Chocolate Drop",
        "id": 546
    },
    {
        "name": "Chocolate Fondue",
        "id": 547
    },
    {
        "name": "Chocolate Hashberry",
        "id": 548
    },
    {
        "name": "Chocolate Kush",
        "id": 549
    },
    {
        "name": "Chocolate OG",
        "id": 551
    },
    {
        "name": "Chocolate Thai",
        "id": 552
    },
    {
        "name": "Chocolate Thunder",
        "id": 553
    },
    {
        "name": "Chocolope",
        "id": 555
    },
    {
        "name": "Chocolope Kush",
        "id": 556
    },
    {
        "name": "Chong Star",
        "id": 557
    },
    {
        "name": "Chronic",
        "id": 558
    },
    {
        "name": "Chronic Thunder",
        "id": 559
    },
    {
        "name": "Chucky's Bride",
        "id": 560
    },
    {
        "name": "Chunky Diesel",
        "id": 561
    },
    {
        "name": "Chupacabra",
        "id": 562
    },
    {
        "name": "Church OG",
        "id": 563
    },
    {
        "name": "Cinderella 99",
        "id": 564
    },
    {
        "name": "Cinderella's Dream",
        "id": 565
    },
    {
        "name": "Cindy White",
        "id": 566
    },
    {
        "name": "Cinex",
        "id": 567
    },
    {
        "name": "Cirrus",
        "id": 568
    },
    {
        "name": "Citrix",
        "id": 569
    },
    {
        "name": "Citrus Kush",
        "id": 570
    },
    {
        "name": "Citrus Punch",
        "id": 571
    },
    {
        "name": "Citrus Sap",
        "id": 572
    },
    {
        "name": "Citrus Sunshine Haze",
        "id": 573
    },
    {
        "name": "Clementine",
        "id": 574
    },
    {
        "name": "Clementine Kush",
        "id": 575
    },
    {
        "name": "Clockwork Orange",
        "id": 576
    },
    {
        "name": "Cloud 9",
        "id": 577
    },
    {
        "name": "Club 69",
        "id": 578
    },
    {
        "name": "Cluster Bomb",
        "id": 579
    },
    {
        "name": "Cold Creek Kush",
        "id": 580
    },
    {
        "name": "Colombian Gold",
        "id": 581
    },
    {
        "name": "Colombian Mojito",
        "id": 582
    },
    {
        "name": "Colorado Bubba",
        "id": 583
    },
    {
        "name": "Colorado Chem",
        "id": 584
    },
    {
        "name": "Colorado Clementines",
        "id": 585
    },
    {
        "name": "Commerce City Kush",
        "id": 586
    },
    {
        "name": "Concord Grape Blockhead",
        "id": 587
    },
    {
        "name": "Confidential Cheese",
        "id": 588
    },
    {
        "name": "Connie Chung",
        "id": 590
    },
    {
        "name": "Conspiracy Kush",
        "id": 591
    },
    {
        "name": "Cookie Breath",
        "id": 592
    },
    {
        "name": "Cookie Dough",
        "id": 593
    },
    {
        "name": "Cookie Jar",
        "id": 594
    },
    {
        "name": "Cookie Monster",
        "id": 595
    },
    {
        "name": "Cookie Wreck",
        "id": 596
    },
    {
        "name": "Cookies Kush",
        "id": 597
    },
    {
        "name": "Cookies and Cream",
        "id": 598
    },
    {
        "name": "Cookies and Dream",
        "id": 599
    },
    {
        "name": "Copper Kush",
        "id": 600
    },
    {
        "name": "Corleone Kush",
        "id": 601
    },
    {
        "name": "Cornbread",
        "id": 602
    },
    {
        "name": "Cosmic Collision",
        "id": 603
    },
    {
        "name": "Cotton Candy Kush",
        "id": 604
    },
    {
        "name": "Cotton Purple Chem",
        "id": 605
    },
    {
        "name": "Cracker Jack",
        "id": 606
    },
    {
        "name": "Crater Lake",
        "id": 607
    },
    {
        "name": "Crazy Miss Hyde",
        "id": 608
    },
    {
        "name": "Cream Caramel",
        "id": 609
    },
    {
        "name": "Crimea Blue",
        "id": 610
    },
    {
        "name": "Critical 47",
        "id": 611
    },
    {
        "name": "Critical Bilbo",
        "id": 612
    },
    {
        "name": "Critical Cheese",
        "id": 613
    },
    {
        "name": "Critical Haze",
        "id": 614
    },
    {
        "name": "Critical Hog",
        "id": 615
    },
    {
        "name": "Critical Jack",
        "id": 616
    },
    {
        "name": "Critical Kali Mist",
        "id": 617
    },
    {
        "name": "Critical Kush",
        "id": 618
    },
    {
        "name": "Critical Mass",
        "id": 619
    },
    {
        "name": "Critical Plus",
        "id": 620
    },
    {
        "name": "Critical Plus 2.0",
        "id": 621
    },
    {
        "name": "Critical Sensi Star",
        "id": 622
    },
    {
        "name": "Critical Widow",
        "id": 624
    },
    {
        "name": "Critters Cookies",
        "id": 625
    },
    {
        "name": "Crosswalker",
        "id": 627
    },
    {
        "name": "Crouching Tiger Hidden Alien",
        "id": 628
    },
    {
        "name": "Crown OG",
        "id": 629
    },
    {
        "name": "Crown Royale",
        "id": 630
    },
    {
        "name": "Crunch Berry Kush",
        "id": 631
    },
    {
        "name": "Crypt",
        "id": 632
    },
    {
        "name": "Crystal Coma",
        "id": 633
    },
    {
        "name": "Crystal Gayle",
        "id": 634
    },
    {
        "name": "Crystalberry",
        "id": 635
    },
    {
        "name": "Cuvee",
        "id": 636
    },
    {
        "name": "DJ Short Blueberry",
        "id": 638
    },
    {
        "name": "DJ Smoke",
        "id": 639
    },
    {
        "name": "Dairy Queen",
        "id": 640
    },
    {
        "name": "Dakini  Kush",
        "id": 641
    },
    {
        "name": "Damn Sour",
        "id": 642
    },
    {
        "name": "Damnesia",
        "id": 643
    },
    {
        "name": "Dance World",
        "id": 644
    },
    {
        "name": "Dancehall",
        "id": 645
    },
    {
        "name": "Dank Schrader",
        "id": 646
    },
    {
        "name": "Dank Sinatra",
        "id": 647
    },
    {
        "name": "Danky Doodle",
        "id": 648
    },
    {
        "name": "Dark Blue Dream",
        "id": 649
    },
    {
        "name": "Dark Side of the Moon",
        "id": 650
    },
    {
        "name": "Dark Star",
        "id": 651
    },
    {
        "name": "Darkside OG",
        "id": 652
    },
    {
        "name": "Darth Vader OG",
        "id": 653
    },
    {
        "name": "Dawg's Waltz",
        "id": 654
    },
    {
        "name": "Dawgfather OG",
        "id": 655
    },
    {
        "name": "Day Tripper",
        "id": 656
    },
    {
        "name": "Deadhead OG",
        "id": 657
    },
    {
        "name": "Deadwood",
        "id": 658
    },
    {
        "name": "Death Bubba",
        "id": 659
    },
    {
        "name": "Death Star",
        "id": 660
    },
    {
        "name": "Death Star OG",
        "id": 661
    },
    {
        "name": "Deep Cheese",
        "id": 662
    },
    {
        "name": "Deep Chunk",
        "id": 663
    },
    {
        "name": "Deep Purple",
        "id": 664
    },
    {
        "name": "Deep Sleep",
        "id": 665
    },
    {
        "name": "DelaHaze",
        "id": 666
    },
    {
        "name": "Denver Maple",
        "id": 667
    },
    {
        "name": "Desert Star",
        "id": 668
    },
    {
        "name": "Destroyer",
        "id": 669
    },
    {
        "name": "Devil Fruit",
        "id": 670
    },
    {
        "name": "Devil's Tit",
        "id": 671
    },
    {
        "name": "Diablo",
        "id": 672
    },
    {
        "name": "Diamond OG",
        "id": 673
    },
    {
        "name": "Diamond Valley Kush",
        "id": 675
    },
    {
        "name": "Diesel Duff",
        "id": 676
    },
    {
        "name": "Dieseltonic",
        "id": 677
    },
    {
        "name": "Digweed",
        "id": 678
    },
    {
        "name": "Dinachem",
        "id": 679
    },
    {
        "name": "Dirty Girl",
        "id": 680
    },
    {
        "name": "Dirty Widow",
        "id": 682
    },
    {
        "name": "Disney Blue",
        "id": 683
    },
    {
        "name": "Dizzy OG",
        "id": 684
    },
    {
        "name": "Django",
        "id": 685
    },
    {
        "name": "Do-Si-Dos",
        "id": 687
    },
    {
        "name": "Doctor Doctor",
        "id": 688
    },
    {
        "name": "Dog Shit",
        "id": 689
    },
    {
        "name": "Dogwalker OG",
        "id": 690
    },
    {
        "name": "Dolla Sign OG Kush",
        "id": 691
    },
    {
        "name": "Don Shula",
        "id": 693
    },
    {
        "name": "Donegal",
        "id": 694
    },
    {
        "name": "Donna OG",
        "id": 695
    },
    {
        "name": "Doobiebird Daydream",
        "id": 696
    },
    {
        "name": "Doox",
        "id": 697
    },
    {
        "name": "Dopium",
        "id": 698
    },
    {
        "name": "Dorit",
        "id": 699
    },
    {
        "name": "Double Barrel OG",
        "id": 700
    },
    {
        "name": "Double Diesel",
        "id": 701
    },
    {
        "name": "Double Dream",
        "id": 702
    },
    {
        "name": "Double Dutch",
        "id": 703
    },
    {
        "name": "Double OG",
        "id": 705
    },
    {
        "name": "Double Purple Doja",
        "id": 706
    },
    {
        "name": "Double Tangie Banana",
        "id": 707
    },
    {
        "name": "Dr. Feelgood",
        "id": 710
    },
    {
        "name": "Dr. Funk",
        "id": 711
    },
    {
        "name": "Dr. Grinspoon",
        "id": 712
    },
    {
        "name": "Dr. Who",
        "id": 713
    },
    {
        "name": "Dragon OG",
        "id": 714
    },
    {
        "name": "Dragon's Breath",
        "id": 715
    },
    {
        "name": "Dreadlock",
        "id": 716
    },
    {
        "name": "Dream Beaver",
        "id": 717
    },
    {
        "name": "Dream Berry",
        "id": 718
    },
    {
        "name": "Dream Lotus",
        "id": 719
    },
    {
        "name": "Dream Police",
        "id": 720
    },
    {
        "name": "Dream Queen",
        "id": 721
    },
    {
        "name": "Dream Star",
        "id": 722
    },
    {
        "name": "Dreamer’s Glass",
        "id": 723
    },
    {
        "name": "Drizella",
        "id": 724
    },
    {
        "name": "Duke Nukem",
        "id": 725
    },
    {
        "name": "Durban Berry",
        "id": 726
    },
    {
        "name": "Durban Cheese",
        "id": 727
    },
    {
        "name": "Durban Cookies",
        "id": 728
    },
    {
        "name": "Durban Poison",
        "id": 729
    },
    {
        "name": "Durga Mata",
        "id": 730
    },
    {
        "name": "Dutch Crunch",
        "id": 731
    },
    {
        "name": "Dutch Dragon",
        "id": 732
    },
    {
        "name": "Dutch Hawaiian",
        "id": 733
    },
    {
        "name": "Dutch Haze",
        "id": 734
    },
    {
        "name": "Dutch Kush",
        "id": 735
    },
    {
        "name": "Dutch Queen",
        "id": 736
    },
    {
        "name": "Dutch Thunder Fuck",
        "id": 737
    },
    {
        "name": "Dutch Treat",
        "id": 738
    },
    {
        "name": "Dutch Treat Haze",
        "id": 739
    },
    {
        "name": "Dutchberry",
        "id": 740
    },
    {
        "name": "Dynamite",
        "id": 741
    },
    {
        "name": "Early Girl",
        "id": 742
    },
    {
        "name": "Early Miss",
        "id": 743
    },
    {
        "name": "Early Pearl",
        "id": 744
    },
    {
        "name": "Early Skunk",
        "id": 745
    },
    {
        "name": "Earth OG",
        "id": 746
    },
    {
        "name": "Earthquake",
        "id": 747
    },
    {
        "name": "Earthshaker OG",
        "id": 748
    },
    {
        "name": "East Coast Alien",
        "id": 749
    },
    {
        "name": "East Coast Sour Diesel",
        "id": 750
    },
    {
        "name": "Eastern European",
        "id": 751
    },
    {
        "name": "Easy Bud",
        "id": 752
    },
    {
        "name": "Easy Peezy",
        "id": 753
    },
    {
        "name": "Ebola #7",
        "id": 754
    },
    {
        "name": "Ecto Cooler",
        "id": 755
    },
    {
        "name": "Ed Rosenthal Super Bud",
        "id": 756
    },
    {
        "name": "Edelweiss",
        "id": 757
    },
    {
        "name": "El Chapo OG",
        "id": 758
    },
    {
        "name": "El Jefe",
        "id": 759
    },
    {
        "name": "El Niño",
        "id": 760
    },
    {
        "name": "El-Na",
        "id": 761
    },
    {
        "name": "Elderberry Kush",
        "id": 762
    },
    {
        "name": "Electric Black Mamba",
        "id": 763
    },
    {
        "name": "Electric Kool Aid",
        "id": 764
    },
    {
        "name": "Electric Lemon G",
        "id": 765
    },
    {
        "name": "Elephant",
        "id": 766
    },
    {
        "name": "Elphinstone",
        "id": 767
    },
    {
        "name": "Elvis",
        "id": 768
    },
    {
        "name": "Emerald Jack",
        "id": 769
    },
    {
        "name": "Emerald OG",
        "id": 770
    },
    {
        "name": "Emperor Cookie Dough",
        "id": 771
    },
    {
        "name": "Endless Sky",
        "id": 772
    },
    {
        "name": "Enemy of the State",
        "id": 773
    },
    {
        "name": "Enigma",
        "id": 774
    },
    {
        "name": "Eran Almog",
        "id": 775
    },
    {
        "name": "Erez",
        "id": 776
    },
    {
        "name": "Euforia",
        "id": 778
    },
    {
        "name": "Eugene Cream",
        "id": 779
    },
    {
        "name": "Euphoria Cookies",
        "id": 780
    },
    {
        "name": "Everlast",
        "id": 781
    },
    {
        "name": "Ewok",
        "id": 782
    },
    {
        "name": "Exodus Cheese",
        "id": 783
    },
    {
        "name": "Extreme Cream",
        "id": 784
    },
    {
        "name": "Extreme OG",
        "id": 785
    },
    {
        "name": "F'n louZER",
        "id": 786
    },
    {
        "name": "Face Off OG",
        "id": 787
    },
    {
        "name": "Fast Eddy",
        "id": 789
    },
    {
        "name": "Fat Axl",
        "id": 790
    },
    {
        "name": "Fat Nelson",
        "id": 791
    },
    {
        "name": "Fat Purple",
        "id": 792
    },
    {
        "name": "Faygo Red Pop",
        "id": 793
    },
    {
        "name": "Fire Alien Kush",
        "id": 795
    },
    {
        "name": "Fire Haze",
        "id": 796
    },
    {
        "name": "Fire OG",
        "id": 797
    },
    {
        "name": "Firewalker OG",
        "id": 798
    },
    {
        "name": "Flaming Cookies",
        "id": 799
    },
    {
        "name": "Flo",
        "id": 800
    },
    {
        "name": "Flo OG",
        "id": 802
    },
    {
        "name": "Flo Walker",
        "id": 803
    },
    {
        "name": "Flowerbomb Kush",
        "id": 804
    },
    {
        "name": "Flowers For Algernon",
        "id": 805
    },
    {
        "name": "Fortune Cookies",
        "id": 806
    },
    {
        "name": "Four Cups",
        "id": 807
    },
    {
        "name": "Four Star General",
        "id": 808
    },
    {
        "name": "Frank's Gift",
        "id": 810
    },
    {
        "name": "Frankenstein",
        "id": 811
    },
    {
        "name": "Fred Flipn’ Stoned",
        "id": 812
    },
    {
        "name": "Freezeland",
        "id": 813
    },
    {
        "name": "Frida",
        "id": 814
    },
    {
        "name": "Frisian Dew",
        "id": 815
    },
    {
        "name": "Frisian Duck",
        "id": 816
    },
    {
        "name": "Frostbite",
        "id": 817
    },
    {
        "name": "Frosted Freak",
        "id": 818
    },
    {
        "name": "Frosty",
        "id": 819
    },
    {
        "name": "Fruit Loops",
        "id": 820
    },
    {
        "name": "Fruit Punch",
        "id": 821
    },
    {
        "name": "Fruit Spirit",
        "id": 822
    },
    {
        "name": "Fruity Chronic Juice",
        "id": 823
    },
    {
        "name": "Fruity Pebbles",
        "id": 824
    },
    {
        "name": "Fruity Widow",
        "id": 825
    },
    {
        "name": "Fruitylicious",
        "id": 826
    },
    {
        "name": "Fucking Incredible",
        "id": 827
    },
    {
        "name": "Full Moon",
        "id": 828
    },
    {
        "name": "Funfetti",
        "id": 829
    },
    {
        "name": "Funky Monkey",
        "id": 830
    },
    {
        "name": "Future",
        "id": 831
    },
    {
        "name": "G Stik \"Amarillo\"",
        "id": 832
    },
    {
        "name": "G Stik \"Orange\"",
        "id": 833
    },
    {
        "name": "G Stik \"RED\"",
        "id": 834
    },
    {
        "name": "G Stik \"Verde\"",
        "id": 836
    },
    {
        "name": "G.O.A.T.",
        "id": 839
    },
    {
        "name": "G13",
        "id": 840
    },
    {
        "name": "G13 Diesel",
        "id": 841
    },
    {
        "name": "G13 Haze",
        "id": 842
    },
    {
        "name": "G13 Widow",
        "id": 843
    },
    {
        "name": "GI001",
        "id": 844
    },
    {
        "name": "GRiZ Kush",
        "id": 845
    },
    {
        "name": "GTO",
        "id": 846
    },
    {
        "name": "Galactic Jack",
        "id": 847
    },
    {
        "name": "Game Changer",
        "id": 848
    },
    {
        "name": "Gandalf OG",
        "id": 849
    },
    {
        "name": "Ganesh Berry",
        "id": 850
    },
    {
        "name": "Garlic Bud",
        "id": 851
    },
    {
        "name": "Gatekeeper OG",
        "id": 852
    },
    {
        "name": "Gelato",
        "id": 853
    },
    {
        "name": "Gemstone",
        "id": 854
    },
    {
        "name": "Georgia Pine",
        "id": 855
    },
    {
        "name": "Ghost Bubba",
        "id": 856
    },
    {
        "name": "Ghost OG",
        "id": 857
    },
    {
        "name": "Ghost OG Moonshine",
        "id": 858
    },
    {
        "name": "Ghost Ship",
        "id": 859
    },
    {
        "name": "Ghost Train Haze",
        "id": 860
    },
    {
        "name": "Gigabud",
        "id": 861
    },
    {
        "name": "Ginger Ale",
        "id": 862
    },
    {
        "name": "Girl Scout Cookies",
        "id": 863
    },
    {
        "name": "Glad Max",
        "id": 864
    },
    {
        "name": "Glass Apple",
        "id": 865
    },
    {
        "name": "Glass Slipper",
        "id": 866
    },
    {
        "name": "Glue Tech",
        "id": 867
    },
    {
        "name": "Go Time",
        "id": 868
    },
    {
        "name": "Gobbilygoo",
        "id": 869
    },
    {
        "name": "Gobbstopper",
        "id": 870
    },
    {
        "name": "God Bud",
        "id": 871
    },
    {
        "name": "God's Gift",
        "id": 872
    },
    {
        "name": "God's Green Crack",
        "id": 873
    },
    {
        "name": "Godberry",
        "id": 874
    },
    {
        "name": "Godfather OG",
        "id": 875
    },
    {
        "name": "Godfather Purple Kush",
        "id": 876
    },
    {
        "name": "Godzilla",
        "id": 877
    },
    {
        "name": "Godzilla Glue",
        "id": 878
    },
    {
        "name": "God’s Bubba",
        "id": 879
    },
    {
        "name": "God’s Treat",
        "id": 880
    },
    {
        "name": "Gog & Magog",
        "id": 881
    },
    {
        "name": "Goji Diesel",
        "id": 882
    },
    {
        "name": "Goji OG",
        "id": 883
    },
    {
        "name": "Gold King's Thai",
        "id": 884
    },
    {
        "name": "Goldberry",
        "id": 885
    },
    {
        "name": "Golden Gage",
        "id": 886
    },
    {
        "name": "Golden Goat",
        "id": 887
    },
    {
        "name": "Golden Lemon",
        "id": 888
    },
    {
        "name": "Golden Nugget",
        "id": 889
    },
    {
        "name": "Golden Panda",
        "id": 890
    },
    {
        "name": "Golden Pineapple",
        "id": 891
    },
    {
        "name": "Golden Ticket",
        "id": 892
    },
    {
        "name": "Goo",
        "id": 894
    },
    {
        "name": "Gooberry",
        "id": 895
    },
    {
        "name": "Good Medicine",
        "id": 896
    },
    {
        "name": "Gorilla Biscuit",
        "id": 897
    },
    {
        "name": "Gorilla Cookies",
        "id": 898
    },
    {
        "name": "Gorilla Glue #1",
        "id": 899
    },
    {
        "name": "Gorilla Glue #4",
        "id": 900
    },
    {
        "name": "Gorilla Glue #5",
        "id": 901
    },
    {
        "name": "Gorkle",
        "id": 902
    },
    {
        "name": "Goverment Mule",
        "id": 903
    },
    {
        "name": "Grand Doggy Purps",
        "id": 904
    },
    {
        "name": "Grand Hindu",
        "id": 905
    },
    {
        "name": "Grand Hustle",
        "id": 906
    },
    {
        "name": "Granddaddy Purple",
        "id": 907
    },
    {
        "name": "Grandma’s Batch",
        "id": 908
    },
    {
        "name": "Grandpa Larry OG",
        "id": 909
    },
    {
        "name": "Grandpa’s Breath",
        "id": 910
    },
    {
        "name": "Grape Ape",
        "id": 911
    },
    {
        "name": "Grape Calyx",
        "id": 912
    },
    {
        "name": "Grape Cookies",
        "id": 913
    },
    {
        "name": "Grape Crush",
        "id": 914
    },
    {
        "name": "Grape Drink",
        "id": 915
    },
    {
        "name": "Grape God",
        "id": 916
    },
    {
        "name": "Grape Inferno",
        "id": 917
    },
    {
        "name": "Grape Krush",
        "id": 918
    },
    {
        "name": "Grape Kush",
        "id": 919
    },
    {
        "name": "Grape OX",
        "id": 920
    },
    {
        "name": "Grape Skunk",
        "id": 921
    },
    {
        "name": "Grape Stomper",
        "id": 922
    },
    {
        "name": "Grape Valley Kush",
        "id": 923
    },
    {
        "name": "Grapefruit",
        "id": 924
    },
    {
        "name": "Grapefruit Diesel",
        "id": 925
    },
    {
        "name": "Grapefruit Haze",
        "id": 926
    },
    {
        "name": "Grapefruit Kush",
        "id": 927
    },
    {
        "name": "Gravity",
        "id": 928
    },
    {
        "name": "Grease Monkey",
        "id": 929
    },
    {
        "name": "Great White Shark",
        "id": 930
    },
    {
        "name": "Green Avenger",
        "id": 931
    },
    {
        "name": "Green Candy",
        "id": 932
    },
    {
        "name": "Green Cheese",
        "id": 933
    },
    {
        "name": "Green Crack",
        "id": 934
    },
    {
        "name": "Green Crack Extreme",
        "id": 935
    },
    {
        "name": "Green Door Kush",
        "id": 936
    },
    {
        "name": "Green Dragon",
        "id": 937
    },
    {
        "name": "Green Dream",
        "id": 938
    },
    {
        "name": "Green Goblin",
        "id": 939
    },
    {
        "name": "Green Goddess",
        "id": 940
    },
    {
        "name": "Green Haze",
        "id": 941
    },
    {
        "name": "Green Hornet",
        "id": 942
    },
    {
        "name": "Green Kush",
        "id": 943
    },
    {
        "name": "Green Lantern",
        "id": 944
    },
    {
        "name": "Green Love Potion",
        "id": 945
    },
    {
        "name": "Green Mango",
        "id": 946
    },
    {
        "name": "Green Monster",
        "id": 947
    },
    {
        "name": "Green Poison",
        "id": 948
    },
    {
        "name": "Green Python",
        "id": 949
    },
    {
        "name": "Green Queen",
        "id": 950
    },
    {
        "name": "Green Ribbon",
        "id": 951
    },
    {
        "name": "Gremlin",
        "id": 952
    },
    {
        "name": "Grilled Cheese",
        "id": 953
    },
    {
        "name": "Grimace",
        "id": 954
    },
    {
        "name": "Guard Dawg",
        "id": 957
    },
    {
        "name": "Guava Chem",
        "id": 958
    },
    {
        "name": "Guava Kush",
        "id": 959
    },
    {
        "name": "Gucci OG",
        "id": 960
    },
    {
        "name": "Gumbo",
        "id": 962
    },
    {
        "name": "Gummy Bears",
        "id": 963
    },
    {
        "name": "Gupta Haze",
        "id": 964
    },
    {
        "name": "Gupta Kush",
        "id": 965
    },
    {
        "name": "Guptilla",
        "id": 966
    },
    {
        "name": "Gutbuster",
        "id": 967
    },
    {
        "name": "Hades Haze",
        "id": 968
    },
    {
        "name": "Haleigh’s Hope",
        "id": 969
    },
    {
        "name": "Haole",
        "id": 971
    },
    {
        "name": "Haoma",
        "id": 972
    },
    {
        "name": "Haoma Mist",
        "id": 973
    },
    {
        "name": "Hardcore OG",
        "id": 974
    },
    {
        "name": "Harle-Tsu",
        "id": 975
    },
    {
        "name": "Harlequin",
        "id": 976
    },
    {
        "name": "Harley Storm",
        "id": 977
    },
    {
        "name": "Harley Twin",
        "id": 978
    },
    {
        "name": "Harmonia",
        "id": 979
    },
    {
        "name": "Harmony",
        "id": 980
    },
    {
        "name": "Harry Potter",
        "id": 981
    },
    {
        "name": "Hash Plant",
        "id": 982
    },
    {
        "name": "Hashbar OG",
        "id": 983
    },
    {
        "name": "Hashberry",
        "id": 984
    },
    {
        "name": "Hashplant Haze",
        "id": 985
    },
    {
        "name": "Hawaiian",
        "id": 986
    },
    {
        "name": "Hawaiian Cookies",
        "id": 987
    },
    {
        "name": "Hawaiian Delight",
        "id": 988
    },
    {
        "name": "Hawaiian Diesel",
        "id": 989
    },
    {
        "name": "Hawaiian Dream",
        "id": 990
    },
    {
        "name": "Hawaiian Fire",
        "id": 991
    },
    {
        "name": "Hawaiian Haze",
        "id": 992
    },
    {
        "name": "Hawaiian Punch",
        "id": 994
    },
    {
        "name": "Hawaiian Purple Kush",
        "id": 995
    },
    {
        "name": "Hawaiian Sativa",
        "id": 996
    },
    {
        "name": "Hawaiian Skunk",
        "id": 997
    },
    {
        "name": "Hawaiian Snow",
        "id": 998
    },
    {
        "name": "Hawaiian Sunrise",
        "id": 999
    },
    {
        "name": "Hawaiian Thunder Fuck",
        "id": 1000
    },
    {
        "name": "Hawai’i ‘78",
        "id": 1001
    },
    {
        "name": "Haze",
        "id": 1002
    },
    {
        "name": "Haze Berry",
        "id": 1003
    },
    {
        "name": "Haze Heaven",
        "id": 1004
    },
    {
        "name": "Haze Wreck",
        "id": 1006
    },
    {
        "name": "Head Cheese",
        "id": 1007
    },
    {
        "name": "Head Trip",
        "id": 1008
    },
    {
        "name": "Headband",
        "id": 1009
    },
    {
        "name": "Headbanger",
        "id": 1010
    },
    {
        "name": "Heaven Scent",
        "id": 1011
    },
    {
        "name": "Heavy Duty Fruity",
        "id": 1012
    },
    {
        "name": "Heisenberg Kush",
        "id": 1013
    },
    {
        "name": "Hell Raiser OG",
        "id": 1014
    },
    {
        "name": "Hell's Angel OG",
        "id": 1015
    },
    {
        "name": "Hellfire OG",
        "id": 1016
    },
    {
        "name": "Hemlock",
        "id": 1017
    },
    {
        "name": "Hempstar",
        "id": 1018
    },
    {
        "name": "Hercules",
        "id": 1019
    },
    {
        "name": "Herijuana",
        "id": 1020
    },
    {
        "name": "Hibiscus Sunrise",
        "id": 1021
    },
    {
        "name": "Higher Power",
        "id": 1022
    },
    {
        "name": "Highwayman",
        "id": 1023
    },
    {
        "name": "Himalayan Blackberry",
        "id": 1024
    },
    {
        "name": "Himalayan Gold",
        "id": 1025
    },
    {
        "name": "Hindu Kush",
        "id": 1026
    },
    {
        "name": "Hindu Skunk",
        "id": 1027
    },
    {
        "name": "Hippie Chicken",
        "id": 1028
    },
    {
        "name": "Hippie Crippler",
        "id": 1029
    },
    {
        "name": "Hoarfrost",
        "id": 1030
    },
    {
        "name": "Hobbit",
        "id": 1031
    },
    {
        "name": "Hog's Breath",
        "id": 1032
    },
    {
        "name": "Holland's Hope",
        "id": 1033
    },
    {
        "name": "Hollywood OG",
        "id": 1034
    },
    {
        "name": "Holy Ghost",
        "id": 1035
    },
    {
        "name": "Holy Grail Kush",
        "id": 1036
    },
    {
        "name": "Honey Bananas",
        "id": 1037
    },
    {
        "name": "Honey Boo Boo",
        "id": 1038
    },
    {
        "name": "Hong Kong",
        "id": 1040
    },
    {
        "name": "Hoodwreck",
        "id": 1041
    },
    {
        "name": "Huckleberry",
        "id": 1042
    },
    {
        "name": "Huckleberry Hound",
        "id": 1043
    },
    {
        "name": "Hulkamania",
        "id": 1044
    },
    {
        "name": "Humboldt",
        "id": 1045
    },
    {
        "name": "Humboldt Dream",
        "id": 1046
    },
    {
        "name": "Humboldt Headband",
        "id": 1047
    },
    {
        "name": "Hurkle",
        "id": 1048
    },
    {
        "name": "Hurricane",
        "id": 1049
    },
    {
        "name": "Hustler Kush",
        "id": 1050
    },
    {
        "name": "ICED Grapefruit",
        "id": 1051
    },
    {
        "name": "IG Kush",
        "id": 1053
    },
    {
        "name": "Ice",
        "id": 1054
    },
    {
        "name": "Ice Cream",
        "id": 1055
    },
    {
        "name": "Ice Princess",
        "id": 1056
    },
    {
        "name": "Ice Queen",
        "id": 1057
    },
    {
        "name": "Ice Wreck",
        "id": 1058
    },
    {
        "name": "Iced Widow",
        "id": 1059
    },
    {
        "name": "Ill OG",
        "id": 1060
    },
    {
        "name": "Illuminati OG",
        "id": 1061
    },
    {
        "name": "In The Pines",
        "id": 1062
    },
    {
        "name": "Incredible Bulk",
        "id": 1063
    },
    {
        "name": "Incredible Hulk",
        "id": 1064
    },
    {
        "name": "Industrial Plant",
        "id": 1065
    },
    {
        "name": "Ingrid",
        "id": 1066
    },
    {
        "name": "Irene OG",
        "id": 1068
    },
    {
        "name": "Iron Triangle",
        "id": 1070
    },
    {
        "name": "Island Haze",
        "id": 1071
    },
    {
        "name": "Island Maui Haze",
        "id": 1072
    },
    {
        "name": "Island Sweet Skunk",
        "id": 1073
    },
    {
        "name": "J's Famous Kush",
        "id": 1074
    },
    {
        "name": "J-27",
        "id": 1075
    },
    {
        "name": "J1",
        "id": 1076
    },
    {
        "name": "Jack 47",
        "id": 1078
    },
    {
        "name": "Jack Burton",
        "id": 1079
    },
    {
        "name": "Jack Diesel",
        "id": 1080
    },
    {
        "name": "Jack Flash",
        "id": 1081
    },
    {
        "name": "Jack Frost",
        "id": 1082
    },
    {
        "name": "Jack Haze",
        "id": 1083
    },
    {
        "name": "Jack Herer",
        "id": 1084
    },
    {
        "name": "Jack Kush",
        "id": 1085
    },
    {
        "name": "Jack Skellington",
        "id": 1086
    },
    {
        "name": "Jack Smack",
        "id": 1087
    },
    {
        "name": "Jack Widow",
        "id": 1088
    },
    {
        "name": "Jack Wreck",
        "id": 1089
    },
    {
        "name": "Jack the Ripper",
        "id": 1090
    },
    {
        "name": "Jack's Cleaner",
        "id": 1091
    },
    {
        "name": "Jack's Dream",
        "id": 1092
    },
    {
        "name": "Jackalope",
        "id": 1093
    },
    {
        "name": "Jacked-Up",
        "id": 1094
    },
    {
        "name": "Jacky White",
        "id": 1095
    },
    {
        "name": "Jah Kush",
        "id": 1096
    },
    {
        "name": "Jahwaiian",
        "id": 1097
    },
    {
        "name": "Jamaican",
        "id": 1098
    },
    {
        "name": "Jamaican Dream",
        "id": 1099
    },
    {
        "name": "Jamaican Lion",
        "id": 1100
    },
    {
        "name": "Jamaican Pearl",
        "id": 1101
    },
    {
        "name": "Jamba Juice",
        "id": 1102
    },
    {
        "name": "Jane Doe",
        "id": 1103
    },
    {
        "name": "Jasmine",
        "id": 1104
    },
    {
        "name": "Jawa Pie",
        "id": 1105
    },
    {
        "name": "Jazz",
        "id": 1106
    },
    {
        "name": "Jean Guy",
        "id": 1107
    },
    {
        "name": "Jedi Kush",
        "id": 1108
    },
    {
        "name": "Jelly Roll",
        "id": 1109
    },
    {
        "name": "Jenni",
        "id": 1110
    },
    {
        "name": "Jenny Kush",
        "id": 1111
    },
    {
        "name": "Jesse's Girl",
        "id": 1112
    },
    {
        "name": "Jesus",
        "id": 1113
    },
    {
        "name": "Jesus OG",
        "id": 1114
    },
    {
        "name": "Jet Fuel",
        "id": 1115
    },
    {
        "name": "Jillybean",
        "id": 1117
    },
    {
        "name": "Jimi Hendrix",
        "id": 1118
    },
    {
        "name": "Jock Horror",
        "id": 1119
    },
    {
        "name": "Johnny’s Tonic",
        "id": 1120
    },
    {
        "name": "Jolly Rancher",
        "id": 1121
    },
    {
        "name": "Josh D OG",
        "id": 1122
    },
    {
        "name": "Jr",
        "id": 1123
    },
    {
        "name": "Juicy Fruit",
        "id": 1124
    },
    {
        "name": "Juicy Jack",
        "id": 1125
    },
    {
        "name": "Juliet",
        "id": 1126
    },
    {
        "name": "Julius Caesar",
        "id": 1127
    },
    {
        "name": "Jungle Juice",
        "id": 1128
    },
    {
        "name": "Jupiter OG",
        "id": 1129
    },
    {
        "name": "K-Train",
        "id": 1130
    },
    {
        "name": "K1",
        "id": 1131
    },
    {
        "name": "K2",
        "id": 1132
    },
    {
        "name": "KC 33",
        "id": 1133
    },
    {
        "name": "KC 36",
        "id": 1134
    },
    {
        "name": "KT Dawg",
        "id": 1135
    },
    {
        "name": "Kaboom",
        "id": 1136
    },
    {
        "name": "Kahuna",
        "id": 1137
    },
    {
        "name": "Kaia Kush",
        "id": 1138
    },
    {
        "name": "Kalalau Kush",
        "id": 1139
    },
    {
        "name": "Kalashnikova",
        "id": 1140
    },
    {
        "name": "Kali 47",
        "id": 1142
    },
    {
        "name": "Kali China",
        "id": 1143
    },
    {
        "name": "Kali Dog",
        "id": 1144
    },
    {
        "name": "Kali Mist",
        "id": 1145
    },
    {
        "name": "Kandahar",
        "id": 1146
    },
    {
        "name": "Kandy Kush",
        "id": 1147
    },
    {
        "name": "Kaptn’s Grand Dream",
        "id": 1148
    },
    {
        "name": "Karma Bitch",
        "id": 1149
    },
    {
        "name": "Katsu Bubba Kush",
        "id": 1150
    },
    {
        "name": "Katsu Kush",
        "id": 1151
    },
    {
        "name": "Kaua’i Electric",
        "id": 1152
    },
    {
        "name": "Kelly Hill Gold",
        "id": 1153
    },
    {
        "name": "Ken's Kush",
        "id": 1154
    },
    {
        "name": "Kerala Krush",
        "id": 1155
    },
    {
        "name": "Key Lime Pie",
        "id": 1156
    },
    {
        "name": "Khalifa Kush",
        "id": 1157
    },
    {
        "name": "Khufu",
        "id": 1159
    },
    {
        "name": "Kid N' Kookies",
        "id": 1160
    },
    {
        "name": "Kilimanjaro",
        "id": 1161
    },
    {
        "name": "Kill Bill",
        "id": 1162
    },
    {
        "name": "Killer Grape",
        "id": 1163
    },
    {
        "name": "Killer Queen",
        "id": 1164
    },
    {
        "name": "Killing Fields",
        "id": 1165
    },
    {
        "name": "Kimbo Kush",
        "id": 1166
    },
    {
        "name": "King Cake",
        "id": 1167
    },
    {
        "name": "King Kong",
        "id": 1168
    },
    {
        "name": "King Louis XIII",
        "id": 1169
    },
    {
        "name": "King's Bread",
        "id": 1170
    },
    {
        "name": "King's Kush",
        "id": 1171
    },
    {
        "name": "Kiwiskunk",
        "id": 1173
    },
    {
        "name": "Kobain Kush",
        "id": 1174
    },
    {
        "name": "Kona Gold",
        "id": 1175
    },
    {
        "name": "Kong",
        "id": 1176
    },
    {
        "name": "Kool-Aid Smile",
        "id": 1177
    },
    {
        "name": "Kosher Kush",
        "id": 1178
    },
    {
        "name": "Kosher Tangie",
        "id": 1179
    },
    {
        "name": "Kraken",
        "id": 1181
    },
    {
        "name": "Krishna Kush",
        "id": 1182
    },
    {
        "name": "Kryptonite",
        "id": 1183
    },
    {
        "name": "Kuato",
        "id": 1184
    },
    {
        "name": "Kuchi",
        "id": 1185
    },
    {
        "name": "Kurple Fantasy",
        "id": 1186
    },
    {
        "name": "Kush Cleaner",
        "id": 1187
    },
    {
        "name": "Kushadelic",
        "id": 1189
    },
    {
        "name": "Kushage",
        "id": 1190
    },
    {
        "name": "Kushashima",
        "id": 1191
    },
    {
        "name": "Kushberry",
        "id": 1192
    },
    {
        "name": "L'Eagle Eagle",
        "id": 1193
    },
    {
        "name": "L.A.K. Federal Reserve",
        "id": 1195
    },
    {
        "name": "LA Chocolat",
        "id": 1196
    },
    {
        "name": "LA Confidential",
        "id": 1197
    },
    {
        "name": "LA Jack",
        "id": 1198
    },
    {
        "name": "LA Kookies",
        "id": 1199
    },
    {
        "name": "LA Kush",
        "id": 1200
    },
    {
        "name": "LA OG",
        "id": 1201
    },
    {
        "name": "LA Ultra",
        "id": 1202
    },
    {
        "name": "LA Woman",
        "id": 1203
    },
    {
        "name": "LAPD",
        "id": 1204
    },
    {
        "name": "LSD",
        "id": 1205
    },
    {
        "name": "LVPK",
        "id": 1206
    },
    {
        "name": "Lady Liberty",
        "id": 1208
    },
    {
        "name": "Ladyburn 1974",
        "id": 1209
    },
    {
        "name": "Lake of Fire",
        "id": 1210
    },
    {
        "name": "Lamb's Bread",
        "id": 1211
    },
    {
        "name": "Lambo OG",
        "id": 1212
    },
    {
        "name": "Lamborghini",
        "id": 1213
    },
    {
        "name": "Larry Bird Kush",
        "id": 1214
    },
    {
        "name": "Larry OG",
        "id": 1215
    },
    {
        "name": "Lashkar Gah",
        "id": 1217
    },
    {
        "name": "Laughing Buddha",
        "id": 1218
    },
    {
        "name": "Lavender",
        "id": 1219
    },
    {
        "name": "Lavender Haze",
        "id": 1220
    },
    {
        "name": "Lavender Jones",
        "id": 1221
    },
    {
        "name": "Le Silver Royale",
        "id": 1223
    },
    {
        "name": "Lee Roy",
        "id": 1225
    },
    {
        "name": "Legalized OG",
        "id": 1226
    },
    {
        "name": "Legend OG",
        "id": 1227
    },
    {
        "name": "Legendary Lemon",
        "id": 1228
    },
    {
        "name": "Lemon Alien Dawg",
        "id": 1230
    },
    {
        "name": "Lemon Bananas",
        "id": 1231
    },
    {
        "name": "Lemon Bubble",
        "id": 1232
    },
    {
        "name": "Lemon Cake",
        "id": 1233
    },
    {
        "name": "Lemon Daddy",
        "id": 1234
    },
    {
        "name": "Lemon Diesel",
        "id": 1235
    },
    {
        "name": "Lemon Drop",
        "id": 1236
    },
    {
        "name": "Lemon Fire",
        "id": 1237
    },
    {
        "name": "Lemon G",
        "id": 1238
    },
    {
        "name": "Lemon Haze",
        "id": 1239
    },
    {
        "name": "Lemon Jack",
        "id": 1240
    },
    {
        "name": "Lemon Kush",
        "id": 1242
    },
    {
        "name": "Lemon Larry Lavender",
        "id": 1243
    },
    {
        "name": "Lemon Marmalade",
        "id": 1244
    },
    {
        "name": "Lemon Meringue",
        "id": 1245
    },
    {
        "name": "Lemon OG Haze",
        "id": 1246
    },
    {
        "name": "Lemon OG Kush",
        "id": 1247
    },
    {
        "name": "Lemon Pie",
        "id": 1248
    },
    {
        "name": "Lemon Sativa",
        "id": 1249
    },
    {
        "name": "Lemon Skunk",
        "id": 1250
    },
    {
        "name": "Lemon Thai",
        "id": 1251
    },
    {
        "name": "Lemon Walker OG",
        "id": 1252
    },
    {
        "name": "Lemon Wreck",
        "id": 1253
    },
    {
        "name": "Lemonberry",
        "id": 1254
    },
    {
        "name": "Lemonder",
        "id": 1255
    },
    {
        "name": "Lemonhead OG",
        "id": 1256
    },
    {
        "name": "Leonidas",
        "id": 1257
    },
    {
        "name": "Lethal Purple",
        "id": 1258
    },
    {
        "name": "Liberty Haze",
        "id": 1259
    },
    {
        "name": "Lifesaver",
        "id": 1260
    },
    {
        "name": "Light of Jah",
        "id": 1261
    },
    {
        "name": "Lime Green Skunk",
        "id": 1262
    },
    {
        "name": "Lime Haze",
        "id": 1263
    },
    {
        "name": "Lime Purple Mist",
        "id": 1264
    },
    {
        "name": "Lime Skunk",
        "id": 1265
    },
    {
        "name": "Limon",
        "id": 1266
    },
    {
        "name": "Lions Gate",
        "id": 1267
    },
    {
        "name": "Liquid Butter",
        "id": 1268
    },
    {
        "name": "Little Dragon",
        "id": 1270
    },
    {
        "name": "Locktite",
        "id": 1271
    },
    {
        "name": "Locomotion",
        "id": 1272
    },
    {
        "name": "Lodi Dodi",
        "id": 1273
    },
    {
        "name": "Logic Diesel",
        "id": 1274
    },
    {
        "name": "Longbottom Leaf",
        "id": 1275
    },
    {
        "name": "Lost Coast OG",
        "id": 1276
    },
    {
        "name": "Loud Dream",
        "id": 1277
    },
    {
        "name": "Loud Scout",
        "id": 1278
    },
    {
        "name": "Love Potion #1",
        "id": 1279
    },
    {
        "name": "Love Potion #9",
        "id": 1280
    },
    {
        "name": "Lowryder",
        "id": 1281
    },
    {
        "name": "Loyalty",
        "id": 1282
    },
    {
        "name": "Luca Brasi x Sour Diesel",
        "id": 1284
    },
    {
        "name": "Lucid Bolt",
        "id": 1285
    },
    {
        "name": "Lucid Dream",
        "id": 1286
    },
    {
        "name": "Lucky Charms",
        "id": 1287
    },
    {
        "name": "Lucy",
        "id": 1288
    },
    {
        "name": "M-39",
        "id": 1289
    },
    {
        "name": "MILF",
        "id": 1290
    },
    {
        "name": "MK Ultra",
        "id": 1291
    },
    {
        "name": "Mad Dawg",
        "id": 1292
    },
    {
        "name": "Mad Scientist",
        "id": 1293
    },
    {
        "name": "Madagascar",
        "id": 1294
    },
    {
        "name": "Madcow",
        "id": 1295
    },
    {
        "name": "Madman OG",
        "id": 1296
    },
    {
        "name": "Magic Jordan",
        "id": 1299
    },
    {
        "name": "Magnum",
        "id": 1300
    },
    {
        "name": "Magnum PI",
        "id": 1301
    },
    {
        "name": "Mai Tai Cookies",
        "id": 1302
    },
    {
        "name": "Majestic 12",
        "id": 1303
    },
    {
        "name": "Mako Haze",
        "id": 1304
    },
    {
        "name": "Malakoff",
        "id": 1305
    },
    {
        "name": "Malawi",
        "id": 1306
    },
    {
        "name": "Mammoth",
        "id": 1307
    },
    {
        "name": "Mandala #1",
        "id": 1308
    },
    {
        "name": "Mango",
        "id": 1309
    },
    {
        "name": "Mango Dream",
        "id": 1310
    },
    {
        "name": "Mango Haze",
        "id": 1311
    },
    {
        "name": "Mango Kush",
        "id": 1312
    },
    {
        "name": "Mango Tango",
        "id": 1313
    },
    {
        "name": "Mangolicious",
        "id": 1314
    },
    {
        "name": "Manitoba Poison",
        "id": 1315
    },
    {
        "name": "Maple Leaf Indica",
        "id": 1316
    },
    {
        "name": "Maramota #10",
        "id": 1317
    },
    {
        "name": "Marcosus Marshmellow",
        "id": 1318
    },
    {
        "name": "Margaret Cho-G",
        "id": 1319
    },
    {
        "name": "Marionberry Kush",
        "id": 1320
    },
    {
        "name": "Marley's Collie",
        "id": 1321
    },
    {
        "name": "Mars OG",
        "id": 1322
    },
    {
        "name": "Martian Candy",
        "id": 1323
    },
    {
        "name": "Martian Mean Green",
        "id": 1324
    },
    {
        "name": "Master Bubba",
        "id": 1325
    },
    {
        "name": "Master Jedi",
        "id": 1326
    },
    {
        "name": "Master Kush",
        "id": 1327
    },
    {
        "name": "Master OG",
        "id": 1328
    },
    {
        "name": "Master Skunk",
        "id": 1329
    },
    {
        "name": "Master Yoda",
        "id": 1330
    },
    {
        "name": "Matanuska Thunder Fuck",
        "id": 1331
    },
    {
        "name": "Mataro Blue",
        "id": 1332
    },
    {
        "name": "Maui",
        "id": 1335
    },
    {
        "name": "Maui Berry",
        "id": 1336
    },
    {
        "name": "Maui Bubble Gift",
        "id": 1337
    },
    {
        "name": "Maui Citrus Punch",
        "id": 1338
    },
    {
        "name": "Maui Haole",
        "id": 1339
    },
    {
        "name": "Maui Mango Diesel",
        "id": 1340
    },
    {
        "name": "Maui Pineapple Chunk",
        "id": 1341
    },
    {
        "name": "Maui Wowie",
        "id": 1342
    },
    {
        "name": "Mazar I Sharif",
        "id": 1343
    },
    {
        "name": "Mazar Kush",
        "id": 1344
    },
    {
        "name": "Mean Misty",
        "id": 1345
    },
    {
        "name": "MediHaze",
        "id": 1346
    },
    {
        "name": "Medibud",
        "id": 1347
    },
    {
        "name": "Medicine Man",
        "id": 1348
    },
    {
        "name": "Medicine Woman",
        "id": 1349
    },
    {
        "name": "Medusa",
        "id": 1350
    },
    {
        "name": "Mega Jackpot",
        "id": 1351
    },
    {
        "name": "Megalodon",
        "id": 1352
    },
    {
        "name": "Melon Gum",
        "id": 1354
    },
    {
        "name": "Memory Loss",
        "id": 1355
    },
    {
        "name": "Mendo Breath",
        "id": 1356
    },
    {
        "name": "Mendocino Purps",
        "id": 1357
    },
    {
        "name": "Mercury OG",
        "id": 1358
    },
    {
        "name": "Merlot OG",
        "id": 1359
    },
    {
        "name": "Mexican",
        "id": 1360
    },
    {
        "name": "Mexican Sativa",
        "id": 1361
    },
    {
        "name": "Michael Phelps OG",
        "id": 1362
    },
    {
        "name": "Mickey Kush",
        "id": 1363
    },
    {
        "name": "Middlefork",
        "id": 1364
    },
    {
        "name": "Middlefork x Pineapple Express",
        "id": 1365
    },
    {
        "name": "Midnight",
        "id": 1366
    },
    {
        "name": "Milky Way",
        "id": 1367
    },
    {
        "name": "Millennium",
        "id": 1368
    },
    {
        "name": "Mint Chocolate Chip",
        "id": 1369
    },
    {
        "name": "Mission Kush",
        "id": 1370
    },
    {
        "name": "Misty Kush",
        "id": 1371
    },
    {
        "name": "Misty Morning",
        "id": 1372
    },
    {
        "name": "Mob Boss",
        "id": 1373
    },
    {
        "name": "Moby Dick",
        "id": 1374
    },
    {
        "name": "Moloka’i Frost",
        "id": 1375
    },
    {
        "name": "Moloka’i Purpz",
        "id": 1376
    },
    {
        "name": "Money Maker",
        "id": 1377
    },
    {
        "name": "Monkey Paw",
        "id": 1378
    },
    {
        "name": "Monolith",
        "id": 1379
    },
    {
        "name": "Monster Cookies",
        "id": 1380
    },
    {
        "name": "Monster OG",
        "id": 1381
    },
    {
        "name": "Montana Silvertip",
        "id": 1382
    },
    {
        "name": "Moon Cookies",
        "id": 1383
    },
    {
        "name": "Moonshine Haze",
        "id": 1385
    },
    {
        "name": "Moonwalker Kush",
        "id": 1386
    },
    {
        "name": "Moose and Lobsta",
        "id": 1387
    },
    {
        "name": "Morning Star",
        "id": 1388
    },
    {
        "name": "Mossad",
        "id": 1389
    },
    {
        "name": "Mossimo OG",
        "id": 1390
    },
    {
        "name": "Motavation",
        "id": 1391
    },
    {
        "name": "Mother Tongue",
        "id": 1392
    },
    {
        "name": "Mother of Berries",
        "id": 1393
    },
    {
        "name": "Mother's Milk",
        "id": 1394
    },
    {
        "name": "Mother’s Finest",
        "id": 1395
    },
    {
        "name": "Mother’s Helper",
        "id": 1396
    },
    {
        "name": "Mountain Girl",
        "id": 1397
    },
    {
        "name": "Mr. Nice",
        "id": 1398
    },
    {
        "name": "Mr. Tusk",
        "id": 1399
    },
    {
        "name": "Ms. Universe",
        "id": 1400
    },
    {
        "name": "Mt. Cook",
        "id": 1401
    },
    {
        "name": "Mt. Hood Magic",
        "id": 1402
    },
    {
        "name": "Mt. Rainier",
        "id": 1403
    },
    {
        "name": "NL5 Haze Mist",
        "id": 1406
    },
    {
        "name": "NY Cheese",
        "id": 1407
    },
    {
        "name": "NY-47",
        "id": 1408
    },
    {
        "name": "NYC Diesel",
        "id": 1409
    },
    {
        "name": "NYPD",
        "id": 1410
    },
    {
        "name": "Narnia",
        "id": 1411
    },
    {
        "name": "Nebula",
        "id": 1412
    },
    {
        "name": "Nebula II CBD",
        "id": 1413
    },
    {
        "name": "Negra 44",
        "id": 1414
    },
    {
        "name": "Nepalese",
        "id": 1415
    },
    {
        "name": "Neptune Kush",
        "id": 1417
    },
    {
        "name": "Neptune OG",
        "id": 1418
    },
    {
        "name": "Nevil's Wreck",
        "id": 1419
    },
    {
        "name": "Neville's Haze",
        "id": 1420
    },
    {
        "name": "Nexus OG",
        "id": 1421
    },
    {
        "name": "Nice Cherry",
        "id": 1422
    },
    {
        "name": "Nicole Kush",
        "id": 1423
    },
    {
        "name": "Night Nurse",
        "id": 1424
    },
    {
        "name": "Night Terror OG",
        "id": 1425
    },
    {
        "name": "Night Train",
        "id": 1426
    },
    {
        "name": "NightFire OG",
        "id": 1427
    },
    {
        "name": "Nightmare Cookies",
        "id": 1428
    },
    {
        "name": "Nina Limone",
        "id": 1429
    },
    {
        "name": "Ninja Fruit",
        "id": 1430
    },
    {
        "name": "Nordle",
        "id": 1431
    },
    {
        "name": "North American Indica",
        "id": 1432
    },
    {
        "name": "North American Sativa",
        "id": 1433
    },
    {
        "name": "North Indian",
        "id": 1434
    },
    {
        "name": "Northern Berry",
        "id": 1435
    },
    {
        "name": "Northern Lights",
        "id": 1437
    },
    {
        "name": "Northern Lights #5",
        "id": 1438
    },
    {
        "name": "Northern Lights #5 x Haze",
        "id": 1439
    },
    {
        "name": "Northern Skunk",
        "id": 1440
    },
    {
        "name": "Northern Wreck",
        "id": 1441
    },
    {
        "name": "Nova",
        "id": 1442
    },
    {
        "name": "Nuggetry OG",
        "id": 1443
    },
    {
        "name": "Nuken",
        "id": 1444
    },
    {
        "name": "Nurse Jackie",
        "id": 1445
    },
    {
        "name": "OCA’s Cloud 9",
        "id": 1447
    },
    {
        "name": "OCD",
        "id": 1448
    },
    {
        "name": "OG #18",
        "id": 1449
    },
    {
        "name": "OG Cheese",
        "id": 1450
    },
    {
        "name": "OG Chem",
        "id": 1451
    },
    {
        "name": "OG Diesel Kush",
        "id": 1452
    },
    {
        "name": "OG Eddy Lepp",
        "id": 1453
    },
    {
        "name": "OG Kush",
        "id": 1455
    },
    {
        "name": "OG LA Affie",
        "id": 1456
    },
    {
        "name": "OG Los Angeles Kush",
        "id": 1457
    },
    {
        "name": "OG Poison",
        "id": 1458
    },
    {
        "name": "OG Ringo",
        "id": 1459
    },
    {
        "name": "OG Shark",
        "id": 1460
    },
    {
        "name": "OG Skunk",
        "id": 1461
    },
    {
        "name": "OG Strawberry",
        "id": 1462
    },
    {
        "name": "OG Tonic",
        "id": 1463
    },
    {
        "name": "OG Wreck",
        "id": 1464
    },
    {
        "name": "OG's Pearl",
        "id": 1465
    },
    {
        "name": "OGiesel",
        "id": 1466
    },
    {
        "name": "Obama Kush",
        "id": 1467
    },
    {
        "name": "Odyssey",
        "id": 1468
    },
    {
        "name": "Ogre",
        "id": 1469
    },
    {
        "name": "Ogre Berry",
        "id": 1470
    },
    {
        "name": "Old Lyme Connecticut OG",
        "id": 1471
    },
    {
        "name": "Old School OG",
        "id": 1473
    },
    {
        "name": "Old Toby",
        "id": 1474
    },
    {
        "name": "Omega",
        "id": 1475
    },
    {
        "name": "Omega Dawg",
        "id": 1476
    },
    {
        "name": "One to One",
        "id": 1477
    },
    {
        "name": "Opal OG Kush",
        "id": 1478
    },
    {
        "name": "Opium",
        "id": 1479
    },
    {
        "name": "Or",
        "id": 1480
    },
    {
        "name": "Orange Afghani",
        "id": 1481
    },
    {
        "name": "Orange Blossom",
        "id": 1482
    },
    {
        "name": "Orange Bud",
        "id": 1483
    },
    {
        "name": "Orange Cookies",
        "id": 1484
    },
    {
        "name": "Orange Creamsicle",
        "id": 1485
    },
    {
        "name": "Orange Crush",
        "id": 1486
    },
    {
        "name": "Orange Diesel",
        "id": 1487
    },
    {
        "name": "Orange Dream",
        "id": 1488
    },
    {
        "name": "Orange Durban",
        "id": 1489
    },
    {
        "name": "Orange Haze",
        "id": 1490
    },
    {
        "name": "Orange Hill Special",
        "id": 1491
    },
    {
        "name": "Orange Juice",
        "id": 1492
    },
    {
        "name": "Orange Kush",
        "id": 1493
    },
    {
        "name": "Orange Romulan",
        "id": 1494
    },
    {
        "name": "Orange Skunk",
        "id": 1495
    },
    {
        "name": "Orange Turbo",
        "id": 1496
    },
    {
        "name": "Orange Velvet",
        "id": 1497
    },
    {
        "name": "Orange Wreck",
        "id": 1498
    },
    {
        "name": "Oregon Diesel",
        "id": 1499
    },
    {
        "name": "Oregon Pineapple",
        "id": 1500
    },
    {
        "name": "Oregon Pinot Noir",
        "id": 1501
    },
    {
        "name": "Organic Diesel",
        "id": 1502
    },
    {
        "name": "Ortega",
        "id": 1503
    },
    {
        "name": "Oteil’s Egyptian Kush",
        "id": 1504
    },
    {
        "name": "Outer Space",
        "id": 1505
    },
    {
        "name": "Outlaw",
        "id": 1506
    },
    {
        "name": "P-51",
        "id": 1507
    },
    {
        "name": "P-91",
        "id": 1508
    },
    {
        "name": "Pacific Blue",
        "id": 1510
    },
    {
        "name": "Pagoda",
        "id": 1511
    },
    {
        "name": "Pakistan Valley Kush",
        "id": 1512
    },
    {
        "name": "Pakistani Chitral Kush",
        "id": 1513
    },
    {
        "name": "Panama Punch",
        "id": 1515
    },
    {
        "name": "Panama Red",
        "id": 1516
    },
    {
        "name": "Panda OG",
        "id": 1517
    },
    {
        "name": "Pandora's Box",
        "id": 1518
    },
    {
        "name": "Papa's OG",
        "id": 1519
    },
    {
        "name": "Papaya",
        "id": 1520
    },
    {
        "name": "Paris OG",
        "id": 1521
    },
    {
        "name": "Paris XXX",
        "id": 1522
    },
    {
        "name": "Peaches and Cream",
        "id": 1524
    },
    {
        "name": "Pearl Scout Cookies",
        "id": 1525
    },
    {
        "name": "Pele's Fire OG",
        "id": 1526
    },
    {
        "name": "Pennywise",
        "id": 1527
    },
    {
        "name": "Peppermint Cookies",
        "id": 1528
    },
    {
        "name": "Permafrost",
        "id": 1529
    },
    {
        "name": "Petrolia Headstash",
        "id": 1531
    },
    {
        "name": "Peyote Cookies",
        "id": 1532
    },
    {
        "name": "Pez",
        "id": 1534
    },
    {
        "name": "Phantom Arrow",
        "id": 1535
    },
    {
        "name": "Phantom Cookies",
        "id": 1536
    },
    {
        "name": "Phantom OG",
        "id": 1537
    },
    {
        "name": "Phishhead Kush",
        "id": 1538
    },
    {
        "name": "Pie Face OG",
        "id": 1539
    },
    {
        "name": "Pine Cone",
        "id": 1540
    },
    {
        "name": "Pine Tar Kush",
        "id": 1542
    },
    {
        "name": "Pineapple",
        "id": 1543
    },
    {
        "name": "Pineapple Chunk",
        "id": 1544
    },
    {
        "name": "Pineapple Cookies",
        "id": 1545
    },
    {
        "name": "Pineapple Diesel",
        "id": 1546
    },
    {
        "name": "Pineapple Dog Shit",
        "id": 1547
    },
    {
        "name": "Pineapple Express",
        "id": 1548
    },
    {
        "name": "Pineapple Fields",
        "id": 1549
    },
    {
        "name": "Pineapple Haze",
        "id": 1550
    },
    {
        "name": "Pineapple Jack",
        "id": 1551
    },
    {
        "name": "Pineapple Kush",
        "id": 1552
    },
    {
        "name": "Pineapple OG",
        "id": 1553
    },
    {
        "name": "Pineapple Punch",
        "id": 1554
    },
    {
        "name": "Pineapple Purple Skunk",
        "id": 1555
    },
    {
        "name": "Pineapple Purps",
        "id": 1556
    },
    {
        "name": "Pineapple Skunk",
        "id": 1557
    },
    {
        "name": "Pineapple Super Silver Haze",
        "id": 1558
    },
    {
        "name": "Pineapple Thai",
        "id": 1559
    },
    {
        "name": "Pineapple Trainwreck",
        "id": 1560
    },
    {
        "name": "Pink Berry",
        "id": 1561
    },
    {
        "name": "Pink Bubba",
        "id": 1562
    },
    {
        "name": "Pink Champagne",
        "id": 1563
    },
    {
        "name": "Pink Cookies",
        "id": 1564
    },
    {
        "name": "Pink Death Star",
        "id": 1565
    },
    {
        "name": "Pink Hawaiian",
        "id": 1566
    },
    {
        "name": "Pink Kush",
        "id": 1567
    },
    {
        "name": "Pink Lemonade",
        "id": 1568
    },
    {
        "name": "Pink Mango",
        "id": 1569
    },
    {
        "name": "Pink Panther",
        "id": 1570
    },
    {
        "name": "Pink Pez",
        "id": 1571
    },
    {
        "name": "Pink Starburst",
        "id": 1572
    },
    {
        "name": "Pink Sunset",
        "id": 1573
    },
    {
        "name": "Pitbull",
        "id": 1575
    },
    {
        "name": "Platinum Bubba Kush",
        "id": 1576
    },
    {
        "name": "Platinum Girl Scout Cookies",
        "id": 1577
    },
    {
        "name": "Platinum Huckleberry Cookies",
        "id": 1578
    },
    {
        "name": "Platinum Kush",
        "id": 1579
    },
    {
        "name": "Platinum OG",
        "id": 1580
    },
    {
        "name": "Platinum Purple Kush",
        "id": 1581
    },
    {
        "name": "Platinum Wreck",
        "id": 1582
    },
    {
        "name": "Plushberry",
        "id": 1583
    },
    {
        "name": "Pluto Kush",
        "id": 1584
    },
    {
        "name": "Poison Haze",
        "id": 1585
    },
    {
        "name": "Pokie",
        "id": 1586
    },
    {
        "name": "Poochie Love",
        "id": 1587
    },
    {
        "name": "Pop Rox",
        "id": 1588
    },
    {
        "name": "Popcorn Kush",
        "id": 1589
    },
    {
        "name": "Pot of Gold",
        "id": 1590
    },
    {
        "name": "Power Kush",
        "id": 1591
    },
    {
        "name": "Power Plant",
        "id": 1592
    },
    {
        "name": "Pre-98 Bubba Kush",
        "id": 1593
    },
    {
        "name": "Predator Pink",
        "id": 1594
    },
    {
        "name": "Presidential OG",
        "id": 1595
    },
    {
        "name": "Primus",
        "id": 1596
    },
    {
        "name": "Princess’s Tiara",
        "id": 1597
    },
    {
        "name": "Professor Chaos",
        "id": 1598
    },
    {
        "name": "Project Blue Book",
        "id": 1599
    },
    {
        "name": "Proper BHO Shatter",
        "id": 1600
    },
    {
        "name": "Puna Buddaz",
        "id": 1602
    },
    {
        "name": "Puna Budder",
        "id": 1603
    },
    {
        "name": "Punky Lion",
        "id": 1604
    },
    {
        "name": "Pure Afghan",
        "id": 1605
    },
    {
        "name": "Pure Kush",
        "id": 1606
    },
    {
        "name": "Pure OG",
        "id": 1607
    },
    {
        "name": "Pure Power Plant",
        "id": 1608
    },
    {
        "name": "Purple AK-47",
        "id": 1609
    },
    {
        "name": "Purple Afghani",
        "id": 1610
    },
    {
        "name": "Purple Alien OG",
        "id": 1611
    },
    {
        "name": "Purple Arrow",
        "id": 1612
    },
    {
        "name": "Purple Berry",
        "id": 1614
    },
    {
        "name": "Purple Bubba",
        "id": 1615
    },
    {
        "name": "Purple Bud",
        "id": 1616
    },
    {
        "name": "Purple Buddha",
        "id": 1617
    },
    {
        "name": "Purple Bush",
        "id": 1618
    },
    {
        "name": "Purple Candy",
        "id": 1619
    },
    {
        "name": "Purple Champagne",
        "id": 1620
    },
    {
        "name": "Purple Cheddar",
        "id": 1621
    },
    {
        "name": "Purple Cheese",
        "id": 1622
    },
    {
        "name": "Purple Chemdawg",
        "id": 1623
    },
    {
        "name": "Purple Cotton Candy",
        "id": 1624
    },
    {
        "name": "Purple Cow",
        "id": 1625
    },
    {
        "name": "Purple Crack",
        "id": 1626
    },
    {
        "name": "Purple Cream",
        "id": 1627
    },
    {
        "name": "Purple Diesel",
        "id": 1628
    },
    {
        "name": "Purple Dog Shit",
        "id": 1629
    },
    {
        "name": "Purple Dragon",
        "id": 1630
    },
    {
        "name": "Purple Dream",
        "id": 1631
    },
    {
        "name": "Purple Elephant",
        "id": 1632
    },
    {
        "name": "Purple Goat",
        "id": 1633
    },
    {
        "name": "Purple Goo",
        "id": 1634
    },
    {
        "name": "Purple Gorilla",
        "id": 1635
    },
    {
        "name": "Purple Hashplant",
        "id": 1636
    },
    {
        "name": "Purple Haze",
        "id": 1637
    },
    {
        "name": "Purple Headband",
        "id": 1638
    },
    {
        "name": "Purple Hindu Kush",
        "id": 1639
    },
    {
        "name": "Purple Ice",
        "id": 1640
    },
    {
        "name": "Purple Jack",
        "id": 1641
    },
    {
        "name": "Purple Jolly Rancher",
        "id": 1642
    },
    {
        "name": "Purple Kush",
        "id": 1643
    },
    {
        "name": "Purple Martian Kush",
        "id": 1645
    },
    {
        "name": "Purple Maui",
        "id": 1646
    },
    {
        "name": "Purple Monkey Balls",
        "id": 1647
    },
    {
        "name": "Purple Mr. Nice",
        "id": 1648
    },
    {
        "name": "Purple Nepal",
        "id": 1649
    },
    {
        "name": "Purple OG Kush",
        "id": 1650
    },
    {
        "name": "Purple Panty Dropper",
        "id": 1652
    },
    {
        "name": "Purple Paralysis",
        "id": 1653
    },
    {
        "name": "Purple Passion",
        "id": 1654
    },
    {
        "name": "Purple People Eater",
        "id": 1655
    },
    {
        "name": "Purple Power",
        "id": 1657
    },
    {
        "name": "Purple Princess",
        "id": 1658
    },
    {
        "name": "Purple Punch",
        "id": 1659
    },
    {
        "name": "Purple Rhino",
        "id": 1660
    },
    {
        "name": "Purple Sage ",
        "id": 1661
    },
    {
        "name": "Purple Skunk",
        "id": 1662
    },
    {
        "name": "Purple Sour Diesel",
        "id": 1663
    },
    {
        "name": "Purple Star",
        "id": 1664
    },
    {
        "name": "Purple Swish",
        "id": 1665
    },
    {
        "name": "Purple Tangie",
        "id": 1666
    },
    {
        "name": "Purple Tears",
        "id": 1667
    },
    {
        "name": "Purple Thai",
        "id": 1668
    },
    {
        "name": "Purple Tonic",
        "id": 1670
    },
    {
        "name": "Purple Trainwreck",
        "id": 1671
    },
    {
        "name": "Purple Urkle",
        "id": 1672
    },
    {
        "name": "Purple Voodoo",
        "id": 1673
    },
    {
        "name": "Purple Widow",
        "id": 1674
    },
    {
        "name": "Purple Wreck",
        "id": 1675
    },
    {
        "name": "Q3",
        "id": 1676
    },
    {
        "name": "Qleaner",
        "id": 1677
    },
    {
        "name": "Qrazy Train",
        "id": 1678
    },
    {
        "name": "Quantum Kush",
        "id": 1679
    },
    {
        "name": "Quebec Gold",
        "id": 1680
    },
    {
        "name": "Queen's Panties",
        "id": 1681
    },
    {
        "name": "Querkle",
        "id": 1682
    },
    {
        "name": "Queso",
        "id": 1683
    },
    {
        "name": "Quin-N-Tonic",
        "id": 1684
    },
    {
        "name": "Qush",
        "id": 1685
    },
    {
        "name": "R-4",
        "id": 1686
    },
    {
        "name": "Rainbow",
        "id": 1688
    },
    {
        "name": "Rainbow Jones",
        "id": 1689
    },
    {
        "name": "Rare Dankness #1",
        "id": 1690
    },
    {
        "name": "Rare Darkness",
        "id": 1691
    },
    {
        "name": "Raskal OG",
        "id": 1692
    },
    {
        "name": "Raspberry Cough",
        "id": 1693
    },
    {
        "name": "Raspberry Kush",
        "id": 1694
    },
    {
        "name": "Raw Diesel",
        "id": 1695
    },
    {
        "name": "Ray Charles",
        "id": 1696
    },
    {
        "name": "Rebel Berry OG",
        "id": 1697
    },
    {
        "name": "Reclining Buddha",
        "id": 1699
    },
    {
        "name": "Recon",
        "id": 1700
    },
    {
        "name": "Red Cherry Berry",
        "id": 1701
    },
    {
        "name": "Red Congolese",
        "id": 1702
    },
    {
        "name": "Red Diesel",
        "id": 1703
    },
    {
        "name": "Red Dragon",
        "id": 1704
    },
    {
        "name": "Red Dwarf",
        "id": 1705
    },
    {
        "name": "Red Eye OG",
        "id": 1706
    },
    {
        "name": "Red Haze",
        "id": 1707
    },
    {
        "name": "Red Headed Stranger",
        "id": 1708
    },
    {
        "name": "Red Poison",
        "id": 1709
    },
    {
        "name": "Red Widow",
        "id": 1710
    },
    {
        "name": "Redwood Kush",
        "id": 1713
    },
    {
        "name": "Remedy",
        "id": 1714
    },
    {
        "name": "Rene",
        "id": 1715
    },
    {
        "name": "Rene Mist",
        "id": 1716
    },
    {
        "name": "Richie Rich",
        "id": 1717
    },
    {
        "name": "Riddler OG",
        "id": 1718
    },
    {
        "name": "Ringo's Gift",
        "id": 1721
    },
    {
        "name": "Rip City Purps",
        "id": 1722
    },
    {
        "name": "Ripped Bubba",
        "id": 1723
    },
    {
        "name": "Rob Ford Kush",
        "id": 1724
    },
    {
        "name": "Robert Plant",
        "id": 1725
    },
    {
        "name": "Rockbud",
        "id": 1726
    },
    {
        "name": "Rocklock",
        "id": 1727
    },
    {
        "name": "Rockstar",
        "id": 1728
    },
    {
        "name": "Rockstar Kush",
        "id": 1729
    },
    {
        "name": "Rockstar Master Kush",
        "id": 1730
    },
    {
        "name": "Rocky Mountain Blueberry",
        "id": 1732
    },
    {
        "name": "Rocky Mountain High",
        "id": 1733
    },
    {
        "name": "Rollex OG Kush",
        "id": 1734
    },
    {
        "name": "Rolls Choice",
        "id": 1735
    },
    {
        "name": "Romping Goddess",
        "id": 1736
    },
    {
        "name": "Romulan",
        "id": 1737
    },
    {
        "name": "Romulan Grapefruit",
        "id": 1738
    },
    {
        "name": "Romulan Haze",
        "id": 1739
    },
    {
        "name": "Root Beer Kush",
        "id": 1740
    },
    {
        "name": "Rose Bud",
        "id": 1741
    },
    {
        "name": "Royal Chemdawg",
        "id": 1742
    },
    {
        "name": "Royal Dwarf",
        "id": 1743
    },
    {
        "name": "Royal Haze",
        "id": 1744
    },
    {
        "name": "Royal Highness",
        "id": 1745
    },
    {
        "name": "Royal Kush",
        "id": 1746
    },
    {
        "name": "Rubicon",
        "id": 1747
    },
    {
        "name": "RudeBoi OG",
        "id": 1748
    },
    {
        "name": "Rug Burn OG",
        "id": 1749
    },
    {
        "name": "Russian Assassin",
        "id": 1750
    },
    {
        "name": "Russian Rocket Fuel",
        "id": 1751
    },
    {
        "name": "SAGE",
        "id": 1752
    },
    {
        "name": "SFV Dog",
        "id": 1753
    },
    {
        "name": "SFV OG",
        "id": 1754
    },
    {
        "name": "SFV OG Kush",
        "id": 1755
    },
    {
        "name": "Sage N Sour",
        "id": 1756
    },
    {
        "name": "Salmon River OG",
        "id": 1757
    },
    {
        "name": "Santa Maria",
        "id": 1758
    },
    {
        "name": "Santa Sativa",
        "id": 1759
    },
    {
        "name": "Sapphire Scout",
        "id": 1760
    },
    {
        "name": "Sapphire Star",
        "id": 1761
    },
    {
        "name": "Sasquatch Sap",
        "id": 1762
    },
    {
        "name": "Satellite OG",
        "id": 1763
    },
    {
        "name": "Satori",
        "id": 1764
    },
    {
        "name": "Saturn OG",
        "id": 1765
    },
    {
        "name": "Savant's Grail",
        "id": 1767
    },
    {
        "name": "Schrom",
        "id": 1768
    },
    {
        "name": "Scooby Snacks",
        "id": 1769
    },
    {
        "name": "Scott's OG",
        "id": 1770
    },
    {
        "name": "Scout's Honor",
        "id": 1771
    },
    {
        "name": "Seattle Blue",
        "id": 1772
    },
    {
        "name": "Seattle Cough",
        "id": 1773
    },
    {
        "name": "Secret Garden OG",
        "id": 1774
    },
    {
        "name": "Secret Recipe",
        "id": 1775
    },
    {
        "name": "Sensi Skunk",
        "id": 1776
    },
    {
        "name": "Sensi Star",
        "id": 1777
    },
    {
        "name": "Sequoia Strawberry",
        "id": 1778
    },
    {
        "name": "Serious 6",
        "id": 1779
    },
    {
        "name": "Sexxpot",
        "id": 1780
    },
    {
        "name": "Shaman",
        "id": 1781
    },
    {
        "name": "Shangri-La",
        "id": 1782
    },
    {
        "name": "Shark Attack",
        "id": 1783
    },
    {
        "name": "Shark Bite",
        "id": 1784
    },
    {
        "name": "Shark Shock",
        "id": 1785
    },
    {
        "name": "Sharksbreath",
        "id": 1786
    },
    {
        "name": "Shiatsu Kush",
        "id": 1787
    },
    {
        "name": "Shipwreck",
        "id": 1789
    },
    {
        "name": "Shishkaberry",
        "id": 1790
    },
    {
        "name": "Shiskaquine",
        "id": 1791
    },
    {
        "name": "Shiva Skunk",
        "id": 1792
    },
    {
        "name": "Shoreline",
        "id": 1793
    },
    {
        "name": "Shurman #7",
        "id": 1794
    },
    {
        "name": "Sierra Mist",
        "id": 1795
    },
    {
        "name": "Silver Back",
        "id": 1796
    },
    {
        "name": "Silver Bubble",
        "id": 1797
    },
    {
        "name": "Silver Haze",
        "id": 1799
    },
    {
        "name": "Silver Kush",
        "id": 1800
    },
    {
        "name": "Silver Pearl",
        "id": 1802
    },
    {
        "name": "Silver Surfer",
        "id": 1803
    },
    {
        "name": "Silver Train",
        "id": 1804
    },
    {
        "name": "Silverback Gorilla",
        "id": 1805
    },
    {
        "name": "Silverfalls Kush",
        "id": 1806
    },
    {
        "name": "Silverhawks OG",
        "id": 1807
    },
    {
        "name": "Sin City Kush",
        "id": 1808
    },
    {
        "name": "Sin Valley OG",
        "id": 1809
    },
    {
        "name": "SinMint Cookies",
        "id": 1810
    },
    {
        "name": "Sirius Black",
        "id": 1812
    },
    {
        "name": "Six Shooter",
        "id": 1813
    },
    {
        "name": "Skins Skunk",
        "id": 1814
    },
    {
        "name": "Skunk 47",
        "id": 1815
    },
    {
        "name": "Skunk Dawg",
        "id": 1816
    },
    {
        "name": "Skunk Haze",
        "id": 1817
    },
    {
        "name": "Skunk No. 1",
        "id": 1818
    },
    {
        "name": "Star Berry Indica",
        "id": 1819
    },
    {
        "name": "Star Killer",
        "id": 1820
    },
    {
        "name": "Star Master Kush",
        "id": 1821
    },
    {
        "name": "Star Tonic",
        "id": 1822
    },
    {
        "name": "StarBud",
        "id": 1823
    },
    {
        "name": "Stardawg",
        "id": 1824
    },
    {
        "name": "Stardawg Guava",
        "id": 1825
    },
    {
        "name": "Starfighter",
        "id": 1826
    },
    {
        "name": "Starry Night",
        "id": 1827
    },
    {
        "name": "Stella Blue",
        "id": 1828
    },
    {
        "name": "Stephen Hawking Kush",
        "id": 1829
    },
    {
        "name": "Stevie Wonder",
        "id": 1830
    },
    {
        "name": "Straight A's Haze",
        "id": 1831
    },
    {
        "name": "Strawberry",
        "id": 1832
    },
    {
        "name": "Strawberry Amnesia",
        "id": 1833
    },
    {
        "name": "Strawberry Banana",
        "id": 1834
    },
    {
        "name": "Strawberry Blondie",
        "id": 1835
    },
    {
        "name": "Strawberry Blue",
        "id": 1836
    },
    {
        "name": "Strawberry Cheesecake",
        "id": 1837
    },
    {
        "name": "Strawberry Cough",
        "id": 1838
    },
    {
        "name": "Strawberry Diesel",
        "id": 1839
    },
    {
        "name": "Strawberry Dream",
        "id": 1840
    },
    {
        "name": "Strawberry Durban Diesel",
        "id": 1841
    },
    {
        "name": "Strawberry Fields",
        "id": 1842
    },
    {
        "name": "Strawberry Frost",
        "id": 1843
    },
    {
        "name": "Strawberry Ice",
        "id": 1844
    },
    {
        "name": "Strawberry Kush",
        "id": 1845
    },
    {
        "name": "Strawberry Lemonade",
        "id": 1846
    },
    {
        "name": "Strawberry Mango Haze",
        "id": 1847
    },
    {
        "name": "Strawberry Milkshake",
        "id": 1848
    },
    {
        "name": "Strawberry OG",
        "id": 1849
    },
    {
        "name": "Strawberry Satori",
        "id": 1850
    },
    {
        "name": "Sublime",
        "id": 1851
    },
    {
        "name": "Sugar Black Rose",
        "id": 1852
    },
    {
        "name": "Sugar Cookie",
        "id": 1853
    },
    {
        "name": "Sugar Kush",
        "id": 1854
    },
    {
        "name": "Sugar Mama",
        "id": 1855
    },
    {
        "name": "Sugar Plum",
        "id": 1857
    },
    {
        "name": "Sugar Punch",
        "id": 1858
    },
    {
        "name": "Sugar Shack",
        "id": 1859
    },
    {
        "name": "Suicide Girl",
        "id": 1860
    },
    {
        "name": "Summertime Squeeze",
        "id": 1861
    },
    {
        "name": "Sumo Grande",
        "id": 1862
    },
    {
        "name": "Sun Ra",
        "id": 1864
    },
    {
        "name": "Sunburn",
        "id": 1865
    },
    {
        "name": "Sunset Haze",
        "id": 1867
    },
    {
        "name": "Sunset Sherbet",
        "id": 1868
    },
    {
        "name": "Sunshine",
        "id": 1869
    },
    {
        "name": "Sunshine #4",
        "id": 1870
    },
    {
        "name": "Sunshine Daydream",
        "id": 1871
    },
    {
        "name": "Supa Don",
        "id": 1872
    },
    {
        "name": "Super  Chronic",
        "id": 1873
    },
    {
        "name": "Super Blue Dream",
        "id": 1874
    },
    {
        "name": "Super Bud",
        "id": 1875
    },
    {
        "name": "Super Cat Piss",
        "id": 1876
    },
    {
        "name": "Super Cheese",
        "id": 1877
    },
    {
        "name": "Super G",
        "id": 1878
    },
    {
        "name": "Super Green Crack",
        "id": 1879
    },
    {
        "name": "Super Jack",
        "id": 1880
    },
    {
        "name": "Super Kush",
        "id": 1881
    },
    {
        "name": "Super Lemon Haze",
        "id": 1882
    },
    {
        "name": "Super Lemon OG",
        "id": 1883
    },
    {
        "name": "Super Silver Haze",
        "id": 1884
    },
    {
        "name": "Super Silver Lights",
        "id": 1885
    },
    {
        "name": "Super Skunk",
        "id": 1886
    },
    {
        "name": "Super Snow Dog",
        "id": 1887
    },
    {
        "name": "Super Sour Diesel",
        "id": 1888
    },
    {
        "name": "Super Sour OG",
        "id": 1889
    },
    {
        "name": "Super Sour Skunk",
        "id": 1890
    },
    {
        "name": "Super Sour Widow",
        "id": 1891
    },
    {
        "name": "Super Sweet",
        "id": 1892
    },
    {
        "name": "Supergirl",
        "id": 1893
    },
    {
        "name": "Superglue",
        "id": 1894
    },
    {
        "name": "Superman OG",
        "id": 1895
    },
    {
        "name": "Supermax OG",
        "id": 1896
    },
    {
        "name": "Supernatural",
        "id": 1897
    },
    {
        "name": "Supernova",
        "id": 1898
    },
    {
        "name": "Superstar",
        "id": 1899
    },
    {
        "name": "Suzy Q",
        "id": 1900
    },
    {
        "name": "Swazi Gold",
        "id": 1901
    },
    {
        "name": "Sweet Baby Jane",
        "id": 1902
    },
    {
        "name": "Sweet Berry",
        "id": 1903
    },
    {
        "name": "Sweet Black Angel",
        "id": 1904
    },
    {
        "name": "Sweet Cheese",
        "id": 1905
    },
    {
        "name": "Sweet Deep Grapefruit",
        "id": 1906
    },
    {
        "name": "Sweet Diesel",
        "id": 1907
    },
    {
        "name": "Sweet Dream",
        "id": 1908
    },
    {
        "name": "Sweet Dreams",
        "id": 1909
    },
    {
        "name": "Sweet Harlem Diesel",
        "id": 1910
    },
    {
        "name": "Sweet Jane",
        "id": 1911
    },
    {
        "name": "Sweet Kush",
        "id": 1912
    },
    {
        "name": "Sweet Lafayette",
        "id": 1913
    },
    {
        "name": "Sweet Nina",
        "id": 1914
    },
    {
        "name": "Sweet Pebbles",
        "id": 1915
    },
    {
        "name": "Sweet Tooth",
        "id": 1917
    },
    {
        "name": "Sweet and Sour Widow",
        "id": 1918
    },
    {
        "name": "Swiss Bliss",
        "id": 1919
    },
    {
        "name": "Swiss Gold",
        "id": 1920
    },
    {
        "name": "Swiss Indica",
        "id": 1921
    },
    {
        "name": "Swiss Tsunami",
        "id": 1923
    },
    {
        "name": "THC Bomb",
        "id": 1924
    },
    {
        "name": "TJ's CBD",
        "id": 1925
    },
    {
        "name": "Tahoe Alien",
        "id": 1926
    },
    {
        "name": "Tahoe Hydro Champagne",
        "id": 1928
    },
    {
        "name": "Tahoe Hydro OG",
        "id": 1929
    },
    {
        "name": "Tahoe OG Kush",
        "id": 1930
    },
    {
        "name": "Tahoe Purps",
        "id": 1931
    },
    {
        "name": "Taliban Poison",
        "id": 1932
    },
    {
        "name": "Tangelo",
        "id": 1933
    },
    {
        "name": "Tangelo Kush",
        "id": 1934
    },
    {
        "name": "Tangerine",
        "id": 1936
    },
    {
        "name": "Tangerine Dream",
        "id": 1937
    },
    {
        "name": "Tangerine Haze",
        "id": 1938
    },
    {
        "name": "Tangerine Kush",
        "id": 1939
    },
    {
        "name": "Tangerine Man",
        "id": 1940
    },
    {
        "name": "Tangerine Power",
        "id": 1941
    },
    {
        "name": "Tangerine Sunrise",
        "id": 1942
    },
    {
        "name": "Tangie",
        "id": 1943
    },
    {
        "name": "Tangie Dream",
        "id": 1944
    },
    {
        "name": "Tangier Chilly",
        "id": 1945
    },
    {
        "name": "Tangilope",
        "id": 1946
    },
    {
        "name": "Tango Kush",
        "id": 1947
    },
    {
        "name": "Tembo Kush",
        "id": 1949
    },
    {
        "name": "Terminator OG",
        "id": 1950
    },
    {
        "name": "Tesla",
        "id": 1951
    },
    {
        "name": "Tesla Tower",
        "id": 1952
    },
    {
        "name": "Thai",
        "id": 1953
    },
    {
        "name": "Thai Girl",
        "id": 1954
    },
    {
        "name": "Thai Haze",
        "id": 1955
    },
    {
        "name": "Thai Lights",
        "id": 1956
    },
    {
        "name": "Thai-Tanic",
        "id": 1957
    },
    {
        "name": "The Black",
        "id": 1960
    },
    {
        "name": "The Blintz",
        "id": 1961
    },
    {
        "name": "The Blood",
        "id": 1962
    },
    {
        "name": "The Brotherhood",
        "id": 1963
    },
    {
        "name": "The Cough",
        "id": 1964
    },
    {
        "name": "The Doctor",
        "id": 1965
    },
    {
        "name": "The Flav",
        "id": 1966
    },
    {
        "name": "The Grunk",
        "id": 1968
    },
    {
        "name": "The Hog",
        "id": 1969
    },
    {
        "name": "The OX",
        "id": 1970
    },
    {
        "name": "The One",
        "id": 1971
    },
    {
        "name": "The Ooze",
        "id": 1972
    },
    {
        "name": "The Sauce",
        "id": 1973
    },
    {
        "name": "The Sheriff",
        "id": 1974
    },
    {
        "name": "The Sister",
        "id": 1975
    },
    {
        "name": "The Third Dimension",
        "id": 1976
    },
    {
        "name": "The Truth",
        "id": 1977
    },
    {
        "name": "The Void",
        "id": 1978
    },
    {
        "name": "The White",
        "id": 1979
    },
    {
        "name": "The Wife",
        "id": 1980
    },
    {
        "name": "Thelonious Skunk",
        "id": 1981
    },
    {
        "name": "Thin Mint Girl Scout Cookies",
        "id": 1982
    },
    {
        "name": "Think Different",
        "id": 1983
    },
    {
        "name": "Thor's Hammer",
        "id": 1984
    },
    {
        "name": "Throwback Kush",
        "id": 1985
    },
    {
        "name": "Thunderbird Rose",
        "id": 1986
    },
    {
        "name": "Thunderstruck",
        "id": 1987
    },
    {
        "name": "Tickle Kush",
        "id": 1988
    },
    {
        "name": "Tiger Woods",
        "id": 1989
    },
    {
        "name": "Tiger's Milk",
        "id": 1990
    },
    {
        "name": "Tigermelon",
        "id": 1991
    },
    {
        "name": "Tillamook Strawberry",
        "id": 1992
    },
    {
        "name": "Timewreck",
        "id": 1993
    },
    {
        "name": "Tina Danza",
        "id": 1994
    },
    {
        "name": "Titan's Haze",
        "id": 1995
    },
    {
        "name": "Tokyo OG",
        "id": 1996
    },
    {
        "name": "Tommy's Home Grown",
        "id": 1997
    },
    {
        "name": "Tora Bora",
        "id": 1999
    },
    {
        "name": "Toxic Punch",
        "id": 2000
    },
    {
        "name": "Training Day",
        "id": 2001
    },
    {
        "name": "Trainwreck",
        "id": 2002
    },
    {
        "name": "Tranquil Elephantizer: Remix",
        "id": 2003
    },
    {
        "name": "Trash",
        "id": 2005
    },
    {
        "name": "Travel Joint",
        "id": 2006
    },
    {
        "name": "Treasure Island",
        "id": 2007
    },
    {
        "name": "Tree of Life",
        "id": 2008
    },
    {
        "name": "Tres Dawg",
        "id": 2009
    },
    {
        "name": "Triangle Kush",
        "id": 2010
    },
    {
        "name": "Trident",
        "id": 2011
    },
    {
        "name": "Trifecta",
        "id": 2012
    },
    {
        "name": "Trinity",
        "id": 2013
    },
    {
        "name": "Triple Cheese",
        "id": 2014
    },
    {
        "name": "Triple Diesel",
        "id": 2015
    },
    {
        "name": "Trix",
        "id": 2016
    },
    {
        "name": "Tropic Thunder",
        "id": 2017
    },
    {
        "name": "Tropical Tang",
        "id": 2018
    },
    {
        "name": "Tropicali",
        "id": 2019
    },
    {
        "name": "True OG",
        "id": 2020
    },
    {
        "name": "Truffle Butter",
        "id": 2022
    },
    {
        "name": "Truth Serum",
        "id": 2023
    },
    {
        "name": "Tuna Kush",
        "id": 2024
    },
    {
        "name": "Turbo Mind Warp",
        "id": 2025
    },
    {
        "name": "Tutankhamon",
        "id": 2026
    },
    {
        "name": "Tutti Frutti",
        "id": 2027
    },
    {
        "name": "Twista",
        "id": 2028
    },
    {
        "name": "Twisted Citrus",
        "id": 2029
    },
    {
        "name": "Tyson",
        "id": 2030
    },
    {
        "name": "U2 Kush",
        "id": 2031
    },
    {
        "name": "UK Cheese",
        "id": 2032
    },
    {
        "name": "UW",
        "id": 2033
    },
    {
        "name": "Ultimate Trainwreck",
        "id": 2034
    },
    {
        "name": "Ultra Banana",
        "id": 2035
    },
    {
        "name": "Ultra Sonja",
        "id": 2036
    },
    {
        "name": "Ultra Sour",
        "id": 2037
    },
    {
        "name": "UltraViolet OG",
        "id": 2038
    },
    {
        "name": "Urkle Train Haze",
        "id": 2039
    },
    {
        "name": "VCDC",
        "id": 2040
    },
    {
        "name": "VOTS AZ Super Sour OG",
        "id": 2041
    },
    {
        "name": "Vader OG",
        "id": 2042
    },
    {
        "name": "Valentine X",
        "id": 2043
    },
    {
        "name": "Valley Ghash",
        "id": 2044
    },
    {
        "name": "Valley Girl",
        "id": 2045
    },
    {
        "name": "Vanilla Kush",
        "id": 2046
    },
    {
        "name": "Velvet Bud",
        "id": 2048
    },
    {
        "name": "Venice OG",
        "id": 2050
    },
    {
        "name": "Venom OG",
        "id": 2051
    },
    {
        "name": "Venus OG",
        "id": 2052
    },
    {
        "name": "Verde Electric",
        "id": 2053
    },
    {
        "name": "Versace",
        "id": 2054
    },
    {
        "name": "Very Berry Haze",
        "id": 2055
    },
    {
        "name": "Violator Kush",
        "id": 2057
    },
    {
        "name": "Violet Delight",
        "id": 2058
    },
    {
        "name": "Viper",
        "id": 2059
    },
    {
        "name": "Voodoo",
        "id": 2060
    },
    {
        "name": "Vortex",
        "id": 2061
    },
    {
        "name": "WMD",
        "id": 2062
    },
    {
        "name": "WSU",
        "id": 2063
    },
    {
        "name": "Waipi'o Hapa",
        "id": 2064
    },
    {
        "name": "Waldo",
        "id": 2065
    },
    {
        "name": "Walker Kush",
        "id": 2066
    },
    {
        "name": "Wally OG",
        "id": 2067
    },
    {
        "name": "Walter White",
        "id": 2069
    },
    {
        "name": "Wappa",
        "id": 2070
    },
    {
        "name": "Warlock",
        "id": 2071
    },
    {
        "name": "Watermelon",
        "id": 2072
    },
    {
        "name": "Webster",
        "id": 2073
    },
    {
        "name": "Wedding Cake",
        "id": 2074
    },
    {
        "name": "Wellness OG",
        "id": 2075
    },
    {
        "name": "West Coast Dawg",
        "id": 2076
    },
    {
        "name": "West OG",
        "id": 2077
    },
    {
        "name": "Wet Dream",
        "id": 2078
    },
    {
        "name": "Whitaker Blues",
        "id": 2079
    },
    {
        "name": "White 99",
        "id": 2080
    },
    {
        "name": "White Bastard",
        "id": 2081
    },
    {
        "name": "White Berry",
        "id": 2082
    },
    {
        "name": "White Bubblegum",
        "id": 2083
    },
    {
        "name": "White Buffalo",
        "id": 2084
    },
    {
        "name": "White Caramel Cookie",
        "id": 2085
    },
    {
        "name": "White Cheese",
        "id": 2086
    },
    {
        "name": "White Cookies",
        "id": 2087
    },
    {
        "name": "White Dawg",
        "id": 2088
    },
    {
        "name": "White Diesel",
        "id": 2089
    },
    {
        "name": "White Dragon",
        "id": 2090
    },
    {
        "name": "White Dream",
        "id": 2091
    },
    {
        "name": "White Durban",
        "id": 2092
    },
    {
        "name": "White Elephant",
        "id": 2093
    },
    {
        "name": "White Empress",
        "id": 2094
    },
    {
        "name": "White Fire 43",
        "id": 2095
    },
    {
        "name": "White Fire Alien OG",
        "id": 2096
    },
    {
        "name": "White Fire OG",
        "id": 2097
    },
    {
        "name": "White Gold",
        "id": 2098
    },
    {
        "name": "White Gorilla",
        "id": 2099
    },
    {
        "name": "White Haze",
        "id": 2100
    },
    {
        "name": "White Knight",
        "id": 2101
    },
    {
        "name": "White Kryptonite",
        "id": 2102
    },
    {
        "name": "White Kush",
        "id": 2103
    },
    {
        "name": "White Lavender",
        "id": 2104
    },
    {
        "name": "White Lightning",
        "id": 2105
    },
    {
        "name": "White Lotus",
        "id": 2106
    },
    {
        "name": "White Nightmare",
        "id": 2107
    },
    {
        "name": "White OG",
        "id": 2108
    },
    {
        "name": "White Queen",
        "id": 2109
    },
    {
        "name": "White Rhino",
        "id": 2110
    },
    {
        "name": "White Romulan",
        "id": 2111
    },
    {
        "name": "White Russian",
        "id": 2112
    },
    {
        "name": "White Sangria",
        "id": 2114
    },
    {
        "name": "White Siberian",
        "id": 2115
    },
    {
        "name": "White Skunk",
        "id": 2116
    },
    {
        "name": "White Slipper",
        "id": 2117
    },
    {
        "name": "White Smurf",
        "id": 2118
    },
    {
        "name": "White Strawberry",
        "id": 2119
    },
    {
        "name": "White Urkle",
        "id": 2120
    },
    {
        "name": "White Walker Kush",
        "id": 2121
    },
    {
        "name": "White Widow",
        "id": 2123
    },
    {
        "name": "White Zombie",
        "id": 2124
    },
    {
        "name": "Whiteout",
        "id": 2125
    },
    {
        "name": "Whitewalker OG",
        "id": 2126
    },
    {
        "name": "Wild Thailand",
        "id": 2127
    },
    {
        "name": "Willie Nelson",
        "id": 2128
    },
    {
        "name": "Willy Wonka",
        "id": 2129
    },
    {
        "name": "Willy's Wonder",
        "id": 2130
    },
    {
        "name": "Wonder Haze",
        "id": 2131
    },
    {
        "name": "Wonder Kid",
        "id": 2132
    },
    {
        "name": "Wonder Woman",
        "id": 2133
    },
    {
        "name": "Wonder Woman OG",
        "id": 2134
    },
    {
        "name": "Wonka's Bubblicious",
        "id": 2135
    },
    {
        "name": "Woody Kush",
        "id": 2136
    },
    {
        "name": "Wookie",
        "id": 2137
    },
    {
        "name": "Wookies",
        "id": 2138
    },
    {
        "name": "Wreckage",
        "id": 2139
    },
    {
        "name": "X-Files",
        "id": 2141
    },
    {
        "name": "X-Wing",
        "id": 2142
    },
    {
        "name": "X-tra Chz",
        "id": 2143
    },
    {
        "name": "XJ-13",
        "id": 2144
    },
    {
        "name": "XXX 420",
        "id": 2145
    },
    {
        "name": "XXX OG",
        "id": 2146
    },
    {
        "name": "Xanadu",
        "id": 2147
    },
    {
        "name": "Y Griega",
        "id": 2148
    },
    {
        "name": "Yeti OG",
        "id": 2149
    },
    {
        "name": "Yoda OG",
        "id": 2150
    },
    {
        "name": "Yoda's Brain",
        "id": 2151
    },
    {
        "name": "Yogi Diesel",
        "id": 2152
    },
    {
        "name": "Yumboldt",
        "id": 2153
    },
    {
        "name": "Yummy",
        "id": 2154
    },
    {
        "name": "Zen",
        "id": 2156
    },
    {
        "name": "Zeta Sage",
        "id": 2157
    },
    {
        "name": "Zeus OG",
        "id": 2158
    },
    {
        "name": "Zkittlez",
        "id": 2159
    },
    {
        "name": "Zombie OG",
        "id": 2160
    },
    {
        "name": "Zoom Pie",
        "id": 2161
    },
    {
        "name": "ʻAlenuihāhā",
        "id": 2162
    }
]